package com.codegym.task.task14.task1408;

public class AsianHen extends Hen implements Continent{

    int getMonthlyEggCount(){
        return 30;
    }

    String getDescription(){
        return super.getDescription() + " I come from " + ASIA + ". I lay " + this.getMonthlyEggCount() + " eggs a month.";
    }
}