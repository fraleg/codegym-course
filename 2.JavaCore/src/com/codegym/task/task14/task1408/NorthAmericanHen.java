package com.codegym.task.task14.task1408;

public class NorthAmericanHen extends Hen implements Continent{

    int getMonthlyEggCount(){
        return 40;
    }

    String getDescription(){
        return super.getDescription() + " I come from " + NORTHAMERICA + ". I lay " + this.getMonthlyEggCount() + " eggs a month.";
    }
}