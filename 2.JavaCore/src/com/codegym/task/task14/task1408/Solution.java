package com.codegym.task.task14.task1408;

/* 
Chicken factory

*/

public class Solution {
    public static void main(String[] args) {
        Hen hen = HenFactory.getHen(Continent.AFRICA);
        hen.getMonthlyEggCount();
        //System.out.println(hen.getDescription());
    }

    static class HenFactory {

        static Hen getHen(String continent) {
            Hen hen = null;
            if (continent.equals(Continent.NORTHAMERICA)) {
                return new NorthAmericanHen();
            } else if (continent.equals(Continent.EUROPE)) {
                return new EuropeanHen();
            } else if (continent.equals(Continent.ASIA)) {
                return new AsianHen();
            } else if (continent.equals(Continent.AFRICA)) {
                return new AfricanHen();
            } else return hen;
        }
    }
}
