package com.codegym.task.task14.task1408;

public class EuropeanHen extends Hen implements Continent{

    int getMonthlyEggCount(){
        return 40;
    }

    String getDescription(){
        return super.getDescription() + " I come from " + EUROPE + ". I lay " + this.getMonthlyEggCount() + " eggs a month.";
    }
}