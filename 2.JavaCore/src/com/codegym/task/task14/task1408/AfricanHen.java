package com.codegym.task.task14.task1408;

public class AfricanHen extends Hen implements Continent{

    int getMonthlyEggCount(){
        return 25;
    }

    String getDescription(){
        return super.getDescription() + " I come from " + AFRICA + ". I lay " + this.getMonthlyEggCount() + " eggs a month.";
    }
}