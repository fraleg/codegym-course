package com.codegym.task.task14.task1419;

import java.io.*;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/* 
Exception invasion

*/

public class Solution {
    public static List<Exception> exceptions = new ArrayList<Exception>();

    public static void main(String[] args) {
        initExceptions();

        for (Exception exception : exceptions) {
            System.out.println(exception);
        }
    }

    private static void initExceptions() throws TypeNotPresentException {   // The first exception
        try {
            float i = 1 / 0;

        } catch (ArithmeticException e){
            exceptions.add(e);
            exceptions.add(new NullPointerException());
            exceptions.add(new IndexOutOfBoundsException());
            exceptions.add(new ArrayIndexOutOfBoundsException());
            exceptions.add(new NumberFormatException());
            exceptions.add(new ClassCastException());
            exceptions.add(new IOException());
            exceptions.add(new RuntimeException());
            exceptions.add(new UnsupportedOperationException());
            exceptions.add(new StringIndexOutOfBoundsException());
        }
    }
}
