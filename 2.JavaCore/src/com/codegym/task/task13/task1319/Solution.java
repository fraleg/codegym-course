package com.codegym.task.task13.task1319;

import java.io.*;
import java.util.Scanner;

/* 
Writing to a file from the console

*/

public class Solution {
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        String filename = sc.nextLine();

        File out = new File(filename);
        FileOutputStream fos = new FileOutputStream(out);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

        while (true){
            String line = sc.nextLine();
            bw.write(line);
            bw.newLine();
            if (line.equals("exit")){
                break;
            }
        }

        bw.close();
    }
}
