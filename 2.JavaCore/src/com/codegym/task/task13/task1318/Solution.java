package com.codegym.task.task13.task1318;

import java.io.*;
import java.util.Scanner;

/* 
Reading a file

*/

public class Solution {
    public static void main(String[] args) throws IOException {
        try {
            // Initialize BufferdReader to get filename from user
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            // Get filename from user
            String fileName = reader.readLine();
            //Initialize FileInputStream with user-provided filename
            FileInputStream inStream = new FileInputStream(fileName);

            // Loop to read file and print it to console
            while (inStream.available() > 0) {  //Continue Loop as long as FileInputStream is not at the end of the file
                //int data = inStream.read(); //Int variable data is equal to current byte in file
                char c = (char)inStream.read();
                System.out.print(c); //Print current byte to console
            }

            reader.close();       //Close BufferedReader
            inStream.close();     //Close FileInputStream
        } catch (Exception e) {
        }
    }
}