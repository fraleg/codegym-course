package com.codegym.task.task13.task1326;

/* 
Sorting even numbers from a file

*/

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        String filename = sc.nextLine();
        //String filename2 = "C:/Users/Franciszek/Desktop/numbers.txt";
        ArrayList<Integer> list = new ArrayList<>();

        FileInputStream fis = null;
        BufferedReader reader = null;
        try {
            fis = new FileInputStream(filename);
            reader = new BufferedReader(new InputStreamReader(fis));
            String line = reader.readLine();
            while (line != null){
                int number = Integer.parseInt(line);
                if (number % 2 == 0) {
                    list.add(number);
                }
                line = reader.readLine();
            }
            Collections.sort(list);
            for (Integer i : list){
                System.out.println(i);
            }
            reader.close();
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
