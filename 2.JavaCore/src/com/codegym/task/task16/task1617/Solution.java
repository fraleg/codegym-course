package com.codegym.task.task16.task1617;

/* 
Countdown at the races

*/

public class Solution {
    public static volatile int numSeconds = 8;

    public static void main(String[] args) throws InterruptedException {
        RacingClock clock = new RacingClock();
        Thread.sleep(3500);
        clock.interrupt();
    }

    public static class RacingClock extends Thread {
        public RacingClock() {
            start();
        }

        public void run() {
            try {
                boolean isThree = false;
                while (numSeconds >= 1) {
                    if (numSeconds == 3) {
                        isThree = true;
                    }
                    System.out.print(numSeconds + " ");
                    Thread.sleep(1000);
                    numSeconds--;
                }
                if (isThree) {
                    System.out.print("Go!");
                }
            } catch (InterruptedException e) {
                System.out.print("Interrupted!");
            }
        }
    }
}
