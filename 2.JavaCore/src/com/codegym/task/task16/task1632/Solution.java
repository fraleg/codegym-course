package com.codegym.task.task16.task1632;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Solution {
    public static List<Thread> threads = new ArrayList<>(5);

    static {
        Thread1 t1 = new Thread1();
        threads.add(t1);
        Thread2 t2 = new Thread2();
        threads.add(t2);
        Thread3 t3 = new Thread3();
        threads.add(t3);
        Thread4 t4 = new Thread4();
        threads.add(t4);
        Thread5 t5 = new Thread5();
        threads.add(t5);
    }

    public static void main(String[] args) {
    }

    public static class Thread1 extends Thread {

        public void run() {
            while (true) {

            }
        }
    }

    public static class Thread2 extends Thread {
        @Override
        public void run() {
            try {
                throw new InterruptedException();
            } catch (InterruptedException e) {
                System.out.println("InterruptedException");
            }
        }
    }

    public static class Thread3 extends Thread {
        public void run() {
            while (true) {
                try {
                    System.out.println("Hurray");
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static class Thread4 extends Thread implements Message {

        public void run() {
            while (isAlive()) {
                //showWarning();
            }
        }

        @Override
        public void showWarning() {
            this.stop();
        }
    }

    public static class Thread5 extends Thread {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int sum = 0;
        String line;

        public void run() {
            while (true) {
                try {
                    line = br.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (line.equals("N")){
                    System.out.println(sum);
                    break;
                }
                sum += Integer.parseInt(line);
            }
        }
    }
}