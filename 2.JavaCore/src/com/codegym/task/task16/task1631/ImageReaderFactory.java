package com.codegym.task.task16.task1631;

import com.codegym.task.task16.task1631.common.*;

public class ImageReaderFactory {

    static ImageTypes it;

    public static ImageReader getImageReader(ImageTypes type) {

        if (it == null) {
            //throw new IllegalArgumentException("Unknown image type");
        }

        if (type == ImageTypes.BMP) {
            return new BmpReader();
        } else if (type == ImageTypes.JPG) {
            return new JpgReader();
        } else if (type == ImageTypes.PNG) {
            return new PngReader();
        } else {
            throw new IllegalArgumentException("Unknown image type");
        }
    }
}
