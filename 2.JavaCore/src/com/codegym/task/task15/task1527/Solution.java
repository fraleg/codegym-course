package com.codegym.task.task15.task1527;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.regex.Pattern;

/* 
Request parser

*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String url = br.readLine();
        String removedBeginUrl = url.substring(url.indexOf('?')+1);
        String[] list = removedBeginUrl.split("&");
        Map<String, String> map = new LinkedHashMap<>();

        for (String s : list){
            if (s.contains("=")){
                String[] pair = s.split("=");
                map.put(pair[0], pair[1]);
            } else {
                map.put(s, null);
            }
        }

        Iterator<Map.Entry<String, String>> itr = map.entrySet().iterator();
        while (itr.hasNext()) {
            Map.Entry<String, String> entry = itr.next();
            System.out.print(entry.getKey() + " ");
        }
        System.out.println();

        if (map.containsKey("obj")){
            String val = map.get("obj");
            if (isDouble(val)) {
                alert(Double.parseDouble(val));
            } else {
                alert(val);
            }
        }
    }

    public static boolean isDouble(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static void alert(double value) {
        System.out.println("double: " + value);
    }

    public static void alert(String value) {
        System.out.println("String: " + value);
    }
}
