package com.codegym.task.task15.task1519;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

/* 
Different methods for different types

*/

public class Solution {
    public boolean isNumeric(String s) {
        return s != null && s.matches("[-+]?\\d*\\.?\\d+");
    }
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            String line = br.readLine();

            if (line.equals("exit")) {
                break;
            }

            if (line.matches("[-+]?\\d*\\.?\\d+") && line.contains(".")) {
                Double d = Double.parseDouble(line);
                print(d);
            } else if (line.matches("[-+]?\\d*\\.?\\d+") && !line.contains(".")) {
                Integer i = Integer.parseInt(line);
                if (i <= 0 || i >= 128) {
                    print(i);
                } else {
                    short s = Short.parseShort(line);
                    print(s);
                }
            }
            else {
                print(line);
            }
        }
    }



    public static void print(Double value) {
        System.out.println("This is a Double. Value: " + value);
    }

    public static void print(String value) {
        System.out.println("This is a String. Value: " + value);
    }

    public static void print(short value) {
        System.out.println("This is a short. Value: " + value);
    }

    public static void print(Integer value) {
        System.out.println("This is an Integer. Value: " + value);
    }
}
