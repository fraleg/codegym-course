package com.codegym.task.task15.task1522;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Reinforce the singleton pattern

*/

public class Solution {
    public static void main(String[] args) {

    }

    public static Planet thePlanet;

    static
    {
        try {
            readKeyFromConsoleAndInitPlanet();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void readKeyFromConsoleAndInitPlanet() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String str = br.readLine();

        if (str.equals(Planet.SUN)){
            thePlanet = Sun.getInstance();
        } else if (str.equals(Planet.MOON)){
            thePlanet = Moon.getInstance();
        } else if (str.equals(Planet.EARTH)){
            thePlanet = Earth.getInstance();
        } else {
            thePlanet = null;
        }
    }
}
