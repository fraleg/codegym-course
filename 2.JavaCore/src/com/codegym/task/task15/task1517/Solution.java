package com.codegym.task.task15.task1517;

/* 
Static modifiers and exceptions

*/

public class Solution {
    public static int A = 0;

    static {
        int foo = 0;
        {
            {
                if (Math.sin(3) < 0.5) {
                    throw new ArithmeticException("Heya");
                } else {
                    foo = 3;
                }
            }
        }
    }

    public static int B = 5;

    public static void main(String[] args) {
        System.out.println(B);
    }
}
