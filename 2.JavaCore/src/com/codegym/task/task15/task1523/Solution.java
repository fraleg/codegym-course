package com.codegym.task.task15.task1523;

/* 
Overloading constructors

*/

public class Solution {
    public static void main(String[] args) {

    }

    public Solution() {

    }

    private Solution(String s) {

    }

    protected Solution(Integer i) {

    }

    Solution(Double d) {

    }
}

