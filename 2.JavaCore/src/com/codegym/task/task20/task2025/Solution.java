package com.codegym.task.task20.task2025;

import java.util.LinkedList;

/*
Number algorithms

*/
public class Solution {
    public static long[] getNumbers(long N) {
        LinkedList<Long> list = new LinkedList<>();

        for (long i = 0; i < N; i++) {
            if (check(i)){
                list.add(i);
                writeNumber(i);
            }
        }
        long result[] = new long[list.size()];

        for (int i = 0; i < list.size(); i++){
            result[i] = list.get(i);
        }

        return result;
    }

    public static void main(String[] args) {
       getNumbers(40000);
    }

    public static boolean check(long n) {
        int power = Long.toString(n).length();
        String sLong = Long.toString(n);
        long sum = 0;
        for (int i = 0; i < sLong.length(); i++) {
            sum += Math.pow(Integer.parseInt(String.valueOf(sLong.charAt(i))), power);
        }

        if (sum == n) {
            return true;
        } else
            return false;
    }

    public static void writeNumber(long n) {
        int power = Long.toString(n).length();
        String sLong = Long.toString(n);
        System.out.print(sLong + " = ");
        for (int i = 0; i < sLong.length(); i++) {
            for (int j = 0; j < power; j++) {
                if (j != power - 1) {
                    System.out.print(sLong.charAt(i) + "*");
                } else {
                    System.out.print(sLong.charAt(i));
                }
            }
            if (i != sLong.length() - 1) {
                System.out.print(" + ");
            }
        }
    }
}
