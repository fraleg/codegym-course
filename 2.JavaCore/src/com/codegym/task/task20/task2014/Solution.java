package com.codegym.task.task20.task2014;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Solution implements Serializable {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        //System.out.println(new Solution(4));

        Solution savedObject = new Solution(16);

        String fileName = "C:\\Users\\Franciszek\\Desktop\\file.txt";
        FileOutputStream fos = new FileOutputStream(fileName);
        ObjectOutputStream outputStream = new ObjectOutputStream(fos);
        outputStream.writeObject(savedObject);
        fos.close();
        outputStream.close();


        FileInputStream fis = new FileInputStream(fileName);
        ObjectInputStream objectStream = new ObjectInputStream(fis);
        Object object = objectStream.readObject();
        fis.close();
        objectStream.close();

        Solution loadedObject = (Solution)object;

        System.out.println(savedObject.equals(loadedObject));
    }

    private transient final String pattern = "dd MMMM yyyy, EEEE";
    private transient Date currentDate;
    private transient int temperature;
    String string;

    public Solution(int temperature) {
        this.currentDate = new Date();
        this.temperature = temperature;

        string = "Today is %s, and the current temperature is %s C";
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        this.string = String.format(string, format.format(currentDate), temperature);
    }

    @Override
    public String toString() {
        return this.string;
    }
}
