package com.codegym.task.task20.task2003;

import java.io.*;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/* 
Introducing properties

*/
public class Solution {
    public static Map<String, String> properties = new HashMap<>();

    public void fillInPropertiesMap() throws Exception {
        // Implement this method
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream inputStream = new FileInputStream(bufferedReader.readLine());
        bufferedReader.close();
        Properties prop = new Properties();
        prop.load(inputStream);
        Enumeration<?> e = prop.propertyNames();
        while (e.hasMoreElements()) {
            String key = (String) e.nextElement();
            String value = prop.getProperty(key);
            properties.put(key, value);
        }
        load(inputStream);
        inputStream.close();
    }

    public void save(OutputStream outputStream) throws Exception {
        // Implement this method
        Properties prop = new Properties();
        for (Map.Entry<String, String> pair : properties.entrySet()) {
            String key = pair.getKey();
            String value = pair.getValue();
            prop.setProperty(key, value);
        }
        prop.store(outputStream, null);
        //outputStream.close();
    }

    public void load(InputStream inputStream) throws Exception {
        // Implement this method
        Properties pro = new Properties();
        pro.load(inputStream);
        properties = new HashMap<>();
        Enumeration<?> e = pro.propertyNames();
        while (e.hasMoreElements()) {
            String key = (String) e.nextElement();
            String value = pro.getProperty(key);
            properties.put(key, value);
        }
        //inputStream.close();

    }

    public static void main(String[] args) {

    }
}
