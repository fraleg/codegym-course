package com.codegym.task.task20.task2001;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* 
Reading and writing to a file: Human

*/
public class Solution {
    public static void main(String[] args) {
        // Update the string passed to the createTempFile method based on the path to a file on your hard drive
        try {
            //File yourFile = File.createTempFile("C:\\Users\\Franciszek\\Desktop\\file.txt", null);
            String yourFile = "C:\\Users\\Franciszek\\Desktop\\file.txt";
            OutputStream outputStream = new FileOutputStream(yourFile);
            InputStream inputStream = new FileInputStream(yourFile);

            Human smith = new Human("Smith", new Asset("home", 999_999.99), new Asset("car", 2999.99));
            //Human smith2 = new Human("Smith", new Asset("home", 999_999.99), new Asset("car", 2999.99));
            smith.save(outputStream);
            //smith2.save(outputStream);
            outputStream.flush();

//            for (Asset s : smith.assets) {
//                System.out.println(s.getName());
//                System.out.println(s.getPrice());
//            }
            //System.out.println(smith.assets.get(0).getName());
            Human somePerson = new Human();
            somePerson.load(inputStream);
            inputStream.close();
            // Check that smith is equal to somePerson
            System.out.println(smith.equals(somePerson));

        } catch (IOException e) {
            // e.printStackTrace();
            System.out.println("Oops, something is wrong with my file");
        } catch (Exception e) {
            // e.printStackTrace();
            System.out.println("Oops, something is wrong with the save/load method");
        }
    }

    public static class Human {
        public String name;
        public List<Asset> assets = new ArrayList<>();

        public Human() {
        }

        public Human(String name, Asset... assets) {
            this.name = name;
            if (assets != null) {
                this.assets.addAll(Arrays.asList(assets));
            }
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Human human = (Human) o;

            if (name != null ? !name.equals(human.name) : human.name != null) return false;
            return assets != null ? assets.equals(human.assets) : human.assets == null;
        }

        @Override
        public int hashCode() {
            int result = name != null ? name.hashCode() : 0;
            result = 31 * result + (assets != null ? assets.hashCode() : 0);
            return result;
        }

        public void save(OutputStream outputStream) throws Exception {
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(outputStream));

            if (!name.isEmpty()) {
                bw.write(this.name);
                bw.write("\n");
                if (!this.assets.isEmpty()) {
                    for (Asset s : assets) {
                        bw.write(s.getName());
                        bw.write("\n");
                        bw.write(String.valueOf(s.getPrice()));
                        bw.write("\n");
                    }
                } else {

                }
            }
            bw.close();
            //outputStream.close();
        }

        public void load(InputStream inputStream) throws Exception {
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));

            this.name = br.readLine();

            for (int i = 0; i < 2; i++) {
                String line = br.readLine();
                if (line == null) {
                    break;
                } else {
                    String line2 = br.readLine();
                    assets.add(new Asset(line, Double.parseDouble(line2)));
                }
            }
            br.close();

        }
    }
}
