package com.codegym.task.task19.task1927;

/* 
Contextual advertising

*/

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class Solution {
    public static TestString testString = new TestString();

    public static void main(String[] args) {

        PrintStream oldStream = System.out;
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream adStream = new PrintStream(outputStream);
        System.setOut(adStream);

        testString.printSomething();

        String[] result = outputStream.toString().split("\\n");
        System.setOut(oldStream);
        for (int i = 0; i < result.length; i++) {
            System.out.println(result[i]);
            if (i % 2 == 1) {
                System.out.println("CodeGym - online Java courses");
            }
        }
    }

    public static class TestString {
        public void printSomething() {
            System.out.println("first");
            System.out.println("second");
            System.out.println("third");
            System.out.println("fourth");
            System.out.println("fifth");
        }
    }
}
