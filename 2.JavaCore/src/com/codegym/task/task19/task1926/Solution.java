package com.codegym.task.task19.task1926;

/* 
Mirror image

*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = reader.readLine();
        reader.close();

        BufferedReader br = new BufferedReader(new FileReader(fileName));

        String line = null;
        while ((line = br.readLine()) != null) {
            StringBuilder sb = new StringBuilder();
            sb.append(line);
            sb.reverse();
            System.out.println(sb.toString());
        }

        br.close();
    }
}
