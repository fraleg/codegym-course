package com.codegym.task.task19.task1915;

/* 
Duplicate text

*/

import java.io.*;

public class Solution {
    public static TestString testString = new TestString();

    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String file = reader.readLine();
        reader.close();

        PrintStream conssoleStream = System.out;

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream stream = new PrintStream(outputStream);
        System.setOut(stream);

        testString.printSomething();

        String result = outputStream.toString();

        System.setOut(conssoleStream);

        StringBuilder sb = new StringBuilder(result);
        String s = sb.toString();

        System.out.println(s);

        FileOutputStream fos = new FileOutputStream(file);
        for (int i = 0; i < s.length(); i++) {
            int c = (char)s.charAt(i);
            fos.write(c);
        }
        fos.close();
    }

    public static class TestString {
        public void printSomething() {
            System.out.println("This is text for testing");
        }
    }
}

