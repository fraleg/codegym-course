package com.codegym.task.task19.task1903;

/* 
Adapting multiple interfaces

*/

import java.util.HashMap;
import java.util.Map;

public class Solution {
    public static Map<String, String> countries = new HashMap<>();

    static {
        countries.put("UA", "Ukraine");
        countries.put("US", "United States");
        countries.put("FR", "France");
    }

    public static void main(String[] args) {

//        String str = "501234567";
//        System.out.println(str.substring(0,2));
//        System.out.println(str.substring(2, 5));
//        System.out.println(str.substring(5, 7));
//        System.out.println(str.substring(7, 9));
    }

    public static class IncomeDataAdapter implements Customer, Contact {

        private IncomeData data;

        public IncomeDataAdapter(IncomeData data) {
            this.data = data;
        }

        @Override
        public String getCompanyName() {
            return this.data.getCompany();
        }

        @Override
        public String getCountryName() {
            return countries.get(this.data.getCountryCode());
        }

        @Override
        public String getName() {
            return this.data.getContactLastName() + ", " + this.data.getContactFirstName();
        }

        @Override
        public String getPhoneNumber() {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 10 - Integer.toString(this.data.getPhoneNumber()).length(); i++) {
                sb.append("0");
            }
            sb.append(Integer.toString(this.data.getPhoneNumber()));

            return "+" + Integer.toString(this.data.getCountryPhoneCode()) + "(" + sb.toString().substring(0,3) + ")" +
                    sb.toString().substring(3, 6) + "-" + sb.toString().substring(6, 8) + "-"
                    + sb.toString().substring(8, 10);
        }
    }


    public static interface IncomeData {
        String getCountryCode();        // For example: US

        String getCompany();            // For example: CodeGym Ltd.

        String getContactFirstName();   // For example: John

        String getContactLastName();    // For example: Smith

        int getCountryPhoneCode();      //  For example: 1

        int getPhoneNumber();           // For example: 501234567
    }

    public static interface Customer {
        String getCompanyName();        // For example: CodeGym Ltd.

        String getCountryName();        // For example: United States
    }

    public static interface Contact {
        String getName();               // For example: Smith, John

        String getPhoneNumber();        // For example: +38(050)123-45-67
    }
}