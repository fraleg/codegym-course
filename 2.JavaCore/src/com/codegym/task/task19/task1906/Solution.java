package com.codegym.task.task19.task1906;

/* 
Even characters

*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String file1 = reader.readLine();
        String file2 = reader.readLine();
        reader.close();

        int index = 1;
        FileReader fr = new FileReader(file1);
        FileWriter fw = new FileWriter(file2);

        while (fr.ready()) {
            int data = fr.read();
            if (index % 2 == 0) {
                fw.write(data);
            }
            index++;
        }
        fr.close();
        fw.close();
    }
}
