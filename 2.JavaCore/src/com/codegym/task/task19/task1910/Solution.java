package com.codegym.task.task19.task1910;

/* 
Punctuation

*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String file1 = reader.readLine();
        //String file1 = "C:\\Users\\Franciszek\\Desktop\\file1.txt";
        String file2 = reader.readLine();
        //String file2 = "C:\\Users\\Franciszek\\Desktop\\file2.txt";
        reader.close();

        BufferedReader br = new BufferedReader(new FileReader(file1));
        BufferedWriter bw = new BufferedWriter(new FileWriter(file2));
        String line = null;

        while ((line = br.readLine()) != null) {
            line = line.replaceAll("[^a-zA-Z ]", "");
            line = line.replace("\n", "");
            bw.write(line);
        }

        bw.close();
        br.close();
    }
}
