package com.codegym.task.task19.task1905;

import java.util.HashMap;
import java.util.Map;

/* 
Reinforce the adapter

*/

public class Solution {
    public static Map<String,String> countries = new HashMap<>();

    static {
        countries.put("UA", "Ukraine");
        countries.put("US", "United States");
        countries.put("FR", "France");
    }

    public static void main(String[] args) {
//        String number = "+1(111)222-3333";
//        number = number.replaceAll("[+()-]", "");
//        System.out.println(number);
    }

    public static class DataAdapter implements RowItem {

        private Customer customer;
        private Contact contact;

        public DataAdapter(Customer customer, Contact contact) {
            this.customer = customer;
            this.contact = contact;
        }

        @Override
        public String getCountryCode() {
            String goodKey = "";
            for (Map.Entry<String, String> entry : countries.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                if (value.equals(this.customer.getCountryName())) {
                    goodKey = key;
                }
            }
            return goodKey;
        }

        @Override
        public String getCompany() {
            return this.customer.getCompanyName();
        }

        @Override
        public String getContactFirstName() {
            String firstName = this.contact.getName();
            firstName = firstName.substring(firstName.indexOf(' ') + 1);
            return firstName;
        }

        @Override
        public String getContactLastName() {
            return this.contact.getName().substring(0, this.contact.getName().indexOf(','));
        }

        @Override
        public String getDialString() {
            return "callto://+" + this.contact.getPhoneNumber().replaceAll("[^0-9]", "");
        }
    }

    public static interface RowItem {
        String getCountryCode();        // For example: US
        String getCompany();            // For example: CodeGym Ltd.
        String getContactFirstName();   // For example: John
        String getContactLastName();    // For example: Peterson
        String getDialString();         // For example: callto://+11112223333
    }

    public static interface Customer {
        String getCompanyName();        // For example: CodeGym Ltd.
        String getCountryName();        // For example: United States
    }

    public static interface Contact {
        String getName();               // For example: Peterson, John
        String getPhoneNumber();        // For example: +1(111)222-3333, +3(805)0123-4567, +380(50)123-4567, etc.
    }
}