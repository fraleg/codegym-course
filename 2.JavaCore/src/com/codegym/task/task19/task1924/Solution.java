package com.codegym.task.task19.task1924;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/* 
Replacing numbers

*/

public class Solution {
    public static Map<Integer, String> map = new HashMap<Integer, String>();

    static {
        map.put(0, "zero");
        map.put(1, "one");
        map.put(2, "two");
        map.put(3, "three");
        map.put(4, "four");
        map.put(5, "five");
        map.put(6, "six");
        map.put(7, "seven");
        map.put(8, "eight");
        map.put(9, "nine");
        map.put(10, "ten");
        map.put(11, "eleven");
        map.put(12, "twelve");
    }

    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = reader.readLine();
        reader.close();

        BufferedReader br = new BufferedReader(new FileReader(fileName));
        String line = null;

        while ((line = br.readLine()) != null) {
            String[] words = line.split(" ");

            for (int i = 0; i < words.length; i++) {
                if (words[i].charAt(0) >= 48 && words[i].charAt(0) <= 57) {
                    if (words[i].endsWith(".") || words[i].endsWith(",") || words[i].endsWith("!")) {
                        String lastSign = Character.toString(words[i].charAt(words[i].length() - 1));
                        if (map.containsKey(Integer.parseInt(words[i].substring(0, words[i].length() - 1)))) {
                            words[i] = words[i].replace(words[i], map.get(Integer.parseInt(words[i].substring(0, words[i].length() - 1))));
                            words[i] = words[i] + lastSign;
                        }
                    } else {
                        if (map.containsKey(Integer.parseInt(words[i]))) {
                            words[i] = words[i].replace(words[i], map.get(Integer.parseInt(words[i])));
                        }
                    }
                }
            }
            for (String word : words) {
                System.out.print(word + " ");
            }
            System.out.println();
        }

        br.close();
    }
}