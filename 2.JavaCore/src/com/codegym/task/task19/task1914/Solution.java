package com.codegym.task.task19.task1914;

/* 
Problem solving

*/

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class Solution {
    public static TestString testString = new TestString();

    public static void main(String[] args) {

        PrintStream consoleStream = System.out;

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream stream = new PrintStream(outputStream);
        System.setOut(stream);

        testString.printSomething();

        String result = outputStream.toString();

        System.setOut(consoleStream);

        StringBuilder sb = new StringBuilder(result);
        String s = sb.toString();
        String s2 = sb.toString();

        String first = s.substring(0, s.indexOf(' '));
        s = s.substring(s.indexOf(' ') + 1);

        String op = s.substring(0, s.indexOf(' '));
        s = s.substring(s.indexOf(' ') + 1);

        String second = s.substring(0, s.indexOf(' '));
        s = s.substring(s.indexOf(' ') + 1);

        int score;
        switch (op) {
            case "+":
                score = Integer.parseInt(first) + Integer.parseInt(second);
                System.out.print(s2 + score);
                break;

            case "-":
                score = Integer.parseInt(first) - Integer.parseInt(second);
                System.out.print(s2 + score);
                break;

            case "*":
                score = Integer.parseInt(first) * Integer.parseInt(second);
                System.out.print(s2 + score);
                break;
        }
    }

    public static class TestString {
        public void printSomething() {
            System.out.println("3 + 6 = ");
        }
    }
}

