package com.codegym.task.task19.task1919;

/* 
Calculating salaries

*/

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Solution {
    public static void main(String[] args) throws IOException {

        Map<String, Double> map = new TreeMap<>();
        FileReader fr = new FileReader(args[0]);
        BufferedReader br = new BufferedReader(fr);
        String line = null;

        while ((line = br.readLine()) != null ) {
            String[] pair = line.split(" ");
            String name = pair[0];
            double amount = Double.parseDouble(pair[1]);

            if (map.containsKey(name)) {
                map.replace(name, map.get(name) + amount);
            } else {
                map.put(name, amount);
            }
        }

        for (Map.Entry<String, Double> entry : map.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }

        br.close();
        fr.close();
    }
}
