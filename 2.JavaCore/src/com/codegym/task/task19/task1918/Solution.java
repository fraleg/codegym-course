package com.codegym.task.task19.task1918;

/* 
Introducing tags

*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws IOException {

        String s = "";
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        //String fileName = reader.readLine();
        String fileName = "C:\\Users\\Franciszek\\Desktop\\file1.txt";
        reader.close();

        BufferedReader br = new BufferedReader(new FileReader(fileName));
        String line = null;
        while ((line = br.readLine()) != null) {
            s = s + line;
        }

        String openingTag = "<" + args[0];
        String closingTag = "</" + args[0] + ">";
        String alpha = "α";
        String omega = "Ω";
        s = s.substring(s.indexOf(openingTag));
        s = s.replaceAll(openingTag, alpha);
        s = s.replaceAll(closingTag, omega);


        while (s.contains(alpha)) {
            int countBegin = 0;
            int countEnd = 0;

            for (int i = 0; i < s.length(); i++) {
                if (alpha.equals(String.valueOf(s.charAt(i)))) {
                    countBegin++;
                } else if (omega.equals(String.valueOf(s.charAt(i)))) {
                    countEnd++;
                }

                if (countBegin > 0 && countBegin == countEnd) {
                    String tags = s.substring(0, i + 1);
                    tags = tags.replaceAll(alpha, openingTag);
                    tags = tags.replaceAll(omega, closingTag);
                    System.out.println(tags);
                    s = s.replaceFirst(alpha, "");
                    if (s.contains(alpha)) {
                        s = s.substring(s.indexOf(alpha));
                    }
                }
            }

        }
        br.close();
    }
}
