package com.codegym.task.task19.task1921;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/* 
John Johnson

*/

public class Solution {
    public static final List<Person> PEOPLE = new ArrayList<>();

    public static void main(String[] args) throws IOException, ParseException {

        BufferedReader br = new BufferedReader(new FileReader(args[0]));
        String s = null;

        while ((s = br.readLine()) != null) {
            String sYear = s.substring(s.lastIndexOf(' ') + 1);
            s = s.substring(0, s.lastIndexOf(' '));
            String sDay = s.substring(s.lastIndexOf(' ') + 1);
            s = s.substring(0, s.lastIndexOf(' '));
            String sMonth = s.substring(s.lastIndexOf(' ') + 1);
            s = s.substring(0, s.lastIndexOf(' '));
            String name = s;

            String sdate = sMonth + " " + sDay + " " + sYear;
            Date date = new SimpleDateFormat("MM dd yyyy").parse(sdate);

            PEOPLE.add(new Person(name, date));
        }

        br.close();
    }
}
