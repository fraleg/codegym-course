package com.codegym.task.task19.task1920;

/* 
The richest

*/

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;

public class Solution {
    public static void main(String[] args) throws IOException {

        Map<String, Double> map = new TreeMap<>();
        LinkedList<String> list = new LinkedList<>();
        FileReader fr = new FileReader(args[0]);
        BufferedReader br = new BufferedReader(fr);
        String line = null;
        double max = 0.0;

        while ((line = br.readLine()) != null ) {
            String[] pair = line.split(" ");
            String name = pair[0];
            double amount = Double.parseDouble(pair[1]);

            if (map.containsKey(name)) {
                map.replace(name, map.get(name) + amount);
            } else {
                map.put(name, amount);
            }
        }

        for (Map.Entry<String, Double> entry : map.entrySet()) {
            if (entry.getValue() > max) {
                max = entry.getValue();
            }
        }

        for (Map.Entry<String, Double> entry : map.entrySet()) {
            if (max == entry.getValue()) {
                list.add(entry.getKey());
            }
        }

        for (String s : list) {
            System.out.println(s);
        }

        br.close();
        fr.close();
    }
}
