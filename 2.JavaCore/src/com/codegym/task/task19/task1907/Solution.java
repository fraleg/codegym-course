package com.codegym.task.task19.task1907;

/* 
Counting words

*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = reader.readLine();
        reader.close();

        int countWorld = 0;
        FileReader fr = new FileReader(fileName);
        Scanner scanner = new Scanner(fr);
        scanner.useDelimiter("\\W");

        while (scanner.hasNext()) {
            String data = scanner.next();
            if (data.equals("world")) {
                countWorld++;
            }
        }
        scanner.close();
        fr.close();
        System.out.println(countWorld);
    }
}
