package com.codegym.task.task19.task1908;

/* 
Picking out numbers

*/

import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String file1 = reader.readLine();
        //String file1 = "C:\\Users\\Franciszek\\Desktop\\file1.txt";
        String file2 = reader.readLine();
        //String file2 = "C:\\Users\\Franciszek\\Desktop\\file2.txt";
        reader.close();

        BufferedReader br = new BufferedReader(new FileReader(file1));
        BufferedWriter bw = new BufferedWriter(new FileWriter(file2));
        String line = null;

        while ((line = br.readLine()) != null) {
            StringBuilder sb = new StringBuilder();
            String[] tmp = line.split(" ");
            for (String s : tmp) {
                try {
                    int ifNumber = Integer.parseInt(s.trim());
                    sb.append(ifNumber + " ");
                } catch (Exception ex) {
                }

            }
            bw.write(sb.toString());
        }
        bw.close();
        br.close();
    }
}
