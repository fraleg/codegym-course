package com.codegym.task.task19.task1904;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

/* 
Yet another adapter

*/

public class Solution {

    public static void main(String[] args) {

    }

    public static class PersonScannerAdapter implements PersonScanner {

        private final Scanner fileScanner;

        public PersonScannerAdapter(Scanner scanner) {
            this.fileScanner = scanner;
        }

        @Override
        public Person read() throws IOException {
            String personInfo = fileScanner.nextLine();
            String firstName = personInfo.substring(0, personInfo.indexOf(' '));

            personInfo = personInfo.substring(firstName.length() + 1);
            String middleName = personInfo.substring(0, personInfo.indexOf(' '));

            personInfo = personInfo.substring(middleName.length() + 1);
            String lastName = personInfo.substring(0, personInfo.indexOf(' '));

            String birthDay = personInfo.substring(middleName.length()+1);
            Date date = new Date();
            try {
                date = new SimpleDateFormat("MM dd yyyy", Locale.ENGLISH).parse(birthDay);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            return new Person(firstName, middleName, lastName, date);
        }

        @Override
        public void close() throws IOException {
            this.fileScanner.close();
        }
    }
}
