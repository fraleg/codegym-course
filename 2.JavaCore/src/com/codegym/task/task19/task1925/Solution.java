package com.codegym.task.task19.task1925;

/* 
Long words

*/

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class Solution {
    public static void main(String[] args) throws IOException {

        LinkedList<String> list = new LinkedList<>();
        BufferedReader br = new BufferedReader(new FileReader(args[0]));
        BufferedWriter bw = new BufferedWriter(new FileWriter(args[1]));
        String line = null;

        while ((line = br.readLine()) != null) {
            String[] words = line.split(" ");

            for (String word : words) {
                if (word.length() > 6) {
                    list.add(word);
                }
            }
        }

        for (int i = 0; i < list.size(); i++) {
            if (i == list.size() - 1) {
                bw.write(list.get(i));
            } else {
                bw.write(list.get(i) + ",");
            }
        }

        bw.close();
        br.close();
    }
}
