package com.codegym.task.task19.task1923;

/* 
Words with numbers

*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new FileReader(args[0]));
        BufferedWriter bw = new BufferedWriter(new FileWriter(args[1]));
        String line = null;

        while ((line = br.readLine()) != null) {
            String[] words = line.split(" ");

            for (String word : words) {
                for (int i = 0; i < word.length(); i++) {
                    if (word.charAt(i) >= 48 && word.charAt(i) <= 57) {
                        bw.write(word + " ");
                        break;
                    }
                }
            }
        }

        bw.close();
        br.close();
    }
}
