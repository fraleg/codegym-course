package com.codegym.task.task18.task1805;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Scanner;

/* 
Sorting bytes

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        String filename = scanner.nextLine();
        //String filename = "C:\\Users\\Franciszek\\Desktop\\dupa.txt";
        LinkedList<Integer> list = new LinkedList<>();

        FileInputStream inputStream = new FileInputStream(filename);
        while (inputStream.available() > 0) {
            int data = inputStream.read();
            if (!list.contains(data)) {
                list.add(data);
            }
        }
        inputStream.close();
        Collections.sort(list);
        for (Integer n : list) {
            System.out.print(n + " ");
        }
    }
}
