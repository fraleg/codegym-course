package com.codegym.task.task18.task1819;

/* 
Combining files

*/

import java.io.*;
import java.util.LinkedList;

public class Solution {
    public static void main(String[] args) throws IOException, InterruptedException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String file1 = reader.readLine();
        String file2 = reader.readLine();
        reader.close();
        LinkedList<String> list = new LinkedList<>();

        BufferedReader br = new BufferedReader(new FileReader(file1));
        String line = null;
        while ((line = br.readLine()) != null) {
            list.add(line);
        }
        br.close();

        FileInputStream fis = new FileInputStream(file2);
        FileWriter fw = new FileWriter(file1);
        while (fis.available() > 0) {
            int data = fis.read();
            fw.write(data);
        }

        for(String s : list) {
            fw.write(s);
        }

        fw.close();
        fis.close();

    }
}
