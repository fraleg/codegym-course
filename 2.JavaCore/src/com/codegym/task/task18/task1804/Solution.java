package com.codegym.task.task18.task1804;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/* 
Rarest bytes

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        String filename = scanner.nextLine();
        //String filename = "C:\\Users\\Franciszek\\Desktop\\dupa.txt";
        Map<Integer, Integer> map = new HashMap<>();
        int min = Integer.MAX_VALUE;

        FileInputStream inputStream = new FileInputStream(filename);
        while (inputStream.available() > 0) {
            int data = inputStream.read();
            if (!map.containsKey(data)) {
                map.put(data, 1);
            } else if (map.containsKey(data)){
                map.put(data, map.get(data) + 1);
            }
        }
        inputStream.close();

        for (Integer n : map.values()) {
            if (n < min) {
                min = n;
            }
        }

        for (Integer n : map.keySet()) {
            if(map.get(n).equals(min)) {
                System.out.print(n + " ");
            }
        }
    }
}
