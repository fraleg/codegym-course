package com.codegym.task.task18.task1813;

import java.io.*;

/* 
AmigoOutputStream

*/

public class AmigoOutputStream extends FileOutputStream{
    public static String fileName = "C:\\Users\\Franciszek\\Desktop\\dupa.txt";

    private FileOutputStream outputStream;

    public AmigoOutputStream(FileOutputStream outputStream) throws FileNotFoundException {
        super(fileName);
        this.outputStream = outputStream;
    }

    public void write(byte[] b, int off, int len) throws IOException {
        outputStream.write(b, off, len);
    }

    public void write(byte[] b) throws IOException {
        outputStream.write(b);
    }

    public void write(int b) throws IOException {
        outputStream.write(b);
    }

    public void flush() throws IOException {
        outputStream.flush();
    }

    public void close() throws IOException {
        flush();
        outputStream.write("CodeGym © All rights reserved.".getBytes());
        outputStream.close();
    }

    public static void main(String[] args) throws FileNotFoundException {
        new AmigoOutputStream(new FileOutputStream(fileName));
    }
}
