package com.codegym.task.task18.task1801;

import java.io.FileInputStream;
import java.util.Scanner;

/* 
Maximum byte

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        String filename = scanner.nextLine();
        //String filename = "C:\\Users\\Franciszek\\Desktop\\audi.txt";
        int max = Integer.MIN_VALUE;

        FileInputStream input = new FileInputStream(filename);
        while (input.available() > 0) {
            int data = input.read();
            //System.out.print(data + " ");
            if (data > max) {
                max = data;
            }
        }
        input.close();
        System.out.println(max);
    }
}
