package com.codegym.task.task18.task1812;

import java.io.*;
import java.util.Scanner;

/* 
Extending AmigoOutputStream

*/

public class QuestionFileOutputStream implements AmigoOutputStream {

    private AmigoOutputStream amigo;

    public QuestionFileOutputStream(AmigoOutputStream am) {
        this.amigo = am;
    }

    @Override
    public void flush() throws IOException {
        amigo.flush();
    }

    @Override
    public void write(int b) throws IOException {
        amigo.write(b);
    }

    @Override
    public void write(byte[] b) throws IOException {
        amigo.write(b);
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        amigo.write(b, off, len);
    }

    public void close() throws IOException {
        System.out.println("Do you really want to close the stream? Y/N");
        Scanner sc = new Scanner(System.in);
        String line = sc.nextLine();
        if (line.equals("Y")) {
            amigo.close();
        }
    }
}

