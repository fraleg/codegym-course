package com.codegym.task.task18.task1826;

/* 
Encryption

*/

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Solution {
    public static void main(String[] args) throws IOException {

        String fileName = args[1];
        String fileNameOutput = args[2];

        FileInputStream fis = new FileInputStream(fileName);
        FileOutputStream fos = new FileOutputStream(fileNameOutput);

        switch (args[0]) {
            case "-e":
                while (fis.available() > 0) {
                    int data = fis.read();
                    fos.write(data + 10);
                }
                fis.close();
                fos.close();
                break;
            case "-d":
                while (fis.available() > 0) {
                    int data = fis.read();
                    fos.write(data - 10);
                }
                fis.close();
                fos.close();
                break;
        }
    }

}
