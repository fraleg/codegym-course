package com.codegym.task.task18.task1817;

/* 
Spaces

*/

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Solution {
    public static void main(String[] args) throws IOException {
        FileInputStream inputStream = new FileInputStream(args[0]);
        int countSpaces = 0;
        int allSigns = inputStream.available();

        while (inputStream.available() > 0) {
            int data = inputStream.read();
            if (data == 32) {
                countSpaces++;
            }
        }
        inputStream.close();
        double d = (double)countSpaces/allSigns * 100;
        System.out.printf("%6.2f", d);
    }
}
