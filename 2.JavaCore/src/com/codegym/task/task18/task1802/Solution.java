package com.codegym.task.task18.task1802;

import java.io.FileInputStream;
import java.util.Scanner;

/* 
Minimum byte

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        int min = Integer.MAX_VALUE;
        String filename = scanner.nextLine();

        FileInputStream inputStream = new FileInputStream(filename);

        while (inputStream.available() > 0) {
            int data = inputStream.read();
            if (data < min) {
                min = data;
            }
        }
        inputStream.close();
        System.out.println(min);
    }
}
