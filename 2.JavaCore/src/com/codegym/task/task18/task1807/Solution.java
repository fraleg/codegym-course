package com.codegym.task.task18.task1807;

/* 
Counting commas

*/

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        String filename = scanner.nextLine();
        //String filename = "C:\\Users\\Franciszek\\Desktop\\audi.txt";
        int count = 0;

        FileInputStream inputStream = new FileInputStream(filename);
        while (inputStream.available() > 0) {
            int data = inputStream.read();
            if ((char)data == ',') {
                count++;
            }
        }
        inputStream.close();
        System.out.println(count);
    }
}
