package com.codegym.task.task18.task1809;

/* 
Reversing a file

*/

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        String file1 = scanner.nextLine();
        //String file1 = "C:\\Users\\Franciszek\\Desktop\\dupa.txt";
        String file2 = scanner.nextLine();
        //String file2 = "C:\\Users\\Franciszek\\Desktop\\new1.txt";

        FileInputStream inputStream = new FileInputStream(file1);
        FileOutputStream outputStream = new FileOutputStream(file2);

        while (inputStream.available() > 0) {
            byte[] buffer = new byte[inputStream.available()];
            byte[] buffer2 = new byte[buffer.length];
            int count = inputStream.read(buffer);

            for (int i = buffer.length - 1; i >= 0; i--) {
                buffer2[buffer.length - 1 - i] = buffer[i];
            }
            outputStream.write(buffer2);
        }

        inputStream.close();
        outputStream.close();
    }
}
