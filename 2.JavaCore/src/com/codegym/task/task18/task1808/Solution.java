package com.codegym.task.task18.task1808;

/* 
Splitting a file

*/

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        String file1 = scanner.nextLine();
        //String file1 = "C:\\Users\\Franciszek\\Desktop\\dupa.txt";
        String file2 = scanner.nextLine();
        //String file2 = "C:\\Users\\Franciszek\\Desktop\\new1.txt";
        String file3 = scanner.nextLine();
        //String file3 = "C:\\Users\\Franciszek\\Desktop\\new2.txt";

        FileInputStream inputStream = new FileInputStream(file1);
        FileOutputStream outputStream2 = new FileOutputStream(file2);
        FileOutputStream outputStream3 = new FileOutputStream(file3);

        while (inputStream.available() > 0) {
            byte[] buffer = new byte[inputStream.available()];
            int count = inputStream.read(buffer);
            System.out.println(count);
            int pivot;
            if(count % 2 == 0) {
                pivot = count / 2;
            } else {
                pivot = count / 2 + 1;
            }
            for (byte b : buffer) {
                System.out.print((char)b + " ");
            }
            outputStream2.write(buffer, 0, pivot);
            outputStream3.write(buffer, pivot, count/2);
        }

        inputStream.close();
        outputStream2.close();
        outputStream3.close();
    }
}
