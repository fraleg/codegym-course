package com.codegym.task.task18.task1822;

/* 
Finding data inside a file

*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> listofLines = new ArrayList<>();
        String fileName = reader.readLine();

        BufferedReader br = new BufferedReader(new FileReader(fileName));
        String line = br.readLine();
        while (line != null) {
            listofLines.add(line);
            line = br.readLine();
        }
        br.close();

        for (String s : listofLines) {
            if (s.startsWith(args[0] + " ")) {
                System.out.println(s);
            }
        }

    }
}
