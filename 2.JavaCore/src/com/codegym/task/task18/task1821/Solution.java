package com.codegym.task.task18.task1821;

/* 
Symbol frequency

*/

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Solution {
    public static void main(String[] args) throws IOException {

        Map<Integer, Integer> map = new TreeMap<>();
        FileInputStream inputStream = new FileInputStream(args[0]);
        //FileInputStream inputStream = new FileInputStream("C:\\Users\\Franciszek\\Desktop\\audi.txt");

        while (inputStream.available() > 0) {
            int data = inputStream.read();
            if (map.containsKey(data)) {
                map.put(data, map.get(data) + 1);
            } else {
                map.put(data,1);
            }
        }

        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            int iAsciiValue = entry.getKey();
            String strAsciiTab = Character.toString((char) iAsciiValue);
            System.out.println(strAsciiTab + " " + entry.getValue());
        }
        inputStream.close();

    }
}
