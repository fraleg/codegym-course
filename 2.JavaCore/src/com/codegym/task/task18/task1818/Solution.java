package com.codegym.task.task18.task1818;

/* 
Two in one

*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String file1 = reader.readLine();
        //String file1 = "C:\\Users\\Franciszek\\Desktop\\file1.txt";
        String file2 = reader.readLine();
        //String file2 = "C:\\Users\\Franciszek\\Desktop\\file2.txt";
        String file3 = reader.readLine();
        //String file3 = "C:\\Users\\Franciszek\\Desktop\\file3.txt";
        FileInputStream inputStream2 = new FileInputStream(file2);
        FileInputStream inputStream3 = new FileInputStream(file3);
        FileOutputStream outputStream = new FileOutputStream(file1);

        while (inputStream2.available() > 0) {
            int data = inputStream2.read();
            outputStream.write(data);
        }
        inputStream2.close();
        while (inputStream3.available() > 0) {
            int data = inputStream3.read();
            outputStream.write(data);
        }
        inputStream3.close();
        outputStream.close();

    }
}
