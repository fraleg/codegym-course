package com.codegym.task.task18.task1803;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/* 
Most frequent bytes

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        String filename = scanner.nextLine();
        //String filename = "C:\\Users\\Franciszek\\Desktop\\dupa.txt";
        Map<Integer, Integer> map = new HashMap<>();
        int max = Integer.MIN_VALUE;

        FileInputStream inputStream = new FileInputStream(filename);
        while (inputStream.available() > 0) {
            int data = inputStream.read();
            if (!map.containsKey(data)) {
                map.put(data, 1);
            } else if (map.containsKey(data)){
                map.put(data, map.get(data) + 1);
            }
        }
        inputStream.close();

        for (Integer n : map.values()) {
            if (n > max) {
                max = n;
            }
        }

        for (Integer n : map.keySet()) {
            if(map.get(n).equals(max)) {
                System.out.print(n + " ");
            }
        }
    }
}
