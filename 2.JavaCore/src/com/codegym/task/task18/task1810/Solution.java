package com.codegym.task.task18.task1810;

/* 
DownloadException

*/

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws DownloadException, IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream inputStream = null;
        String filename = "";


        while (true) {
            filename = br.readLine();
            //filename = "C:\\Users\\Franciszek\\Desktop\\audi.txt";
            inputStream = new FileInputStream(filename);
            int count = 0;

            while (inputStream.available() > 0) {
                int data = inputStream.read();
                count++;
            }

            if (count < 1000) {
                inputStream.close();
                throw new DownloadException();
            }
        }
    }

    public static class DownloadException extends Exception {
    }
}
