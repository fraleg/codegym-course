package com.codegym.task.task18.task1824;

/* 
Files and exceptions

*/

import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args){

        Scanner scanner = new Scanner(System.in);
        String fileName;
        FileInputStream fis;

        while (true) {
            fileName = scanner.nextLine();
            try {
                fis = new FileInputStream(fileName);
            } catch (FileNotFoundException e) {
                System.out.println(fileName);
                break;
            }
            try {
                fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
