package com.codegym.task.task18.task1825;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/* 
Building a file

*/

public class Solution {
    public static void main(String[] args) throws IOException {
        LinkedList<String> list = new LinkedList<>();
        String fileName;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream fis;
        FileOutputStream fos;

        while (true) {
            fileName = reader.readLine();
            if (fileName.equals("end")) {
                break;
            }
            fis = new FileInputStream(fileName);
            list.add(fileName);
            fis.close();
        }
        reader.close();

        Collections.sort(list);

        String outputFileName = list.get(0).substring(0, list.get(0).lastIndexOf("part1") - 1);
        fos = new FileOutputStream(outputFileName);
        for (String s : list) {
            fis = new FileInputStream(s);
            byte[] buffer = new byte[fis.available()];
            while (fis.available() > 0) {
                int data = fis.read(buffer);
                fos.write(buffer, 0, data);
            }
            fis.close();
        }
        fos.close();
    }
}
