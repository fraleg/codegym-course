package com.codegym.task.task18.task1820;

/* 
Rounding numbers

*/

import java.io.*;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String file1 = reader.readLine();
        String file2 = reader.readLine();
        //String file1 = "C:\\Users\\Franciszek\\Desktop\\file1.txt";
        //String file2 = "C:\\Users\\Franciszek\\Desktop\\file2.txt";

        FileInputStream inputStream = new FileInputStream(file1);
        FileOutputStream outputStream = new FileOutputStream(file2);
        ArrayList<String> list = new ArrayList<>();
        String element = "";
        while (inputStream.available() > 0) {
            int data = inputStream.read();
            if (data != 32) {
                element += (char)data;
            }
            else {
                list.add(element);
                element = "";
            }
        }
        list.add(element);

        for (String s : list) {
            int number = (int)Math.round(Double.parseDouble(s));
            String text = Integer.toString(number);
            for (int i = 0; i < text.length(); i++) {
                outputStream.write((int)text.charAt(i));
            }
            outputStream.write(32);
        }

        inputStream.close();
        outputStream.close();

    }
}
