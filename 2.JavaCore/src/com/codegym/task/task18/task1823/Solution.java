package com.codegym.task.task18.task1823;

import java.io.*;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/* 
Threads and bytes

*/

public class Solution {
    public static Map<String, Integer> resultMap = new HashMap<String, Integer>();

    public static void main(String[] args) throws IOException {

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String filename = "";
        while (true) {
            filename = bufferedReader.readLine();
            if (filename.equals("exit")) {
                break;
            }
            ReadThread readThread = new ReadThread(filename);
            readThread.start();
        }
        bufferedReader.close();

        for (Map.Entry<String, Integer> mapp : resultMap.entrySet()) {
            System.out.println(mapp.getKey() + " - " + mapp.getValue());
        }
    }

    public static class ReadThread extends Thread {
        LinkedHashMap<Integer, Integer> map = new LinkedHashMap<>();
        //private FileInputStream fis;
        String fileName;

        public ReadThread(String fileName) throws FileNotFoundException {
            //this.fis = new FileInputStream(fileName);
            this.fileName = fileName;
        }

        public void result() throws IOException{
            FileInputStream fis = new FileInputStream(fileName);
            while (fis.available() > 0) {
                int data = fis.read();
                if (map.containsKey(data)) {
                    map.replace(data, map.get(data) + 1);
                } else {
                    map.put(data, 1);
                }
            }
            fis.close();

            // szukamy najczęściej wystepującego znaku
            int max = 0;
            for (Map.Entry<Integer, Integer> mapEntry : map.entrySet()) {
                int value = mapEntry.getValue();
                if (value > max) {
                    max = value;
                }
            }

            // dodajemy do mapy ten znak który występuje najczęściej
            for (Map.Entry<Integer, Integer> mapResult : map.entrySet()) {
                int value = mapResult.getValue();
                if (value == max) {
                    resultMap.put(fileName, mapResult.getKey());
                }
            }
        }

        @Override
        public void run() {
            try {
                result();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
