package com.codegym.task.task12.task1208;

/* 
Freedom of the press

*/

public class Solution {
    public static void main(String[] args) {

    }

    public static String print(String s){
        return s;
    }

    public static int print(int s){
        return s;
    }

    public static double print(double s){
        return s;
    }

    public static byte print(byte s){
        return s;
    }

    public static char print(char s){
        return s;
    }
}
