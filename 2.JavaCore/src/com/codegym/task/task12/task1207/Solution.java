package com.codegym.task.task12.task1207;

/* 
int and Integer

*/

public class Solution {
    public static void main(String[] args) {
        Integer x = new Integer(7);
        int n = 3;
        print(3);
        print(x);
    }

    public static void print(int n){
        System.out.println(n);
    }

    public static void print(Integer n){
        System.out.println(n);
    }
}
