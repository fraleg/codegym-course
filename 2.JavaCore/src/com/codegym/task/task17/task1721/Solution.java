package com.codegym.task.task17.task1721;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/* 
Transactionality

*/

public class Solution {
    public static List<String> allLines = new ArrayList<>();
    public static List<String> linesForRemoval = new ArrayList<>();

    static {
        Scanner sc = new Scanner(System.in);
        BufferedReader br = null;
        String firstFile = sc.nextLine();
        String secondFile = sc.nextLine();

        try {
            br = new BufferedReader(new FileReader(firstFile));
            String line = br.readLine();
            while (line != null) {
                allLines.add(line);
                line = br.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            br = new BufferedReader(new FileReader(secondFile));
            String line = br.readLine();
            while (line != null) {
                linesForRemoval.add(line);
                line = br.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            br.close();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    public static void main(String[] args) throws CorruptedDataException {
        Solution s = new Solution();
        s.joinData();
    }

    public void joinData() throws CorruptedDataException {
        if (allLines.containsAll(linesForRemoval)){
            allLines.removeAll(linesForRemoval);
        } else {
            allLines.clear();
            throw new CorruptedDataException();
        }
    }
}
