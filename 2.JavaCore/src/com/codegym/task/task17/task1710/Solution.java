package com.codegym.task.task17.task1710;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/* 
CRUD

*/

public class Solution {
    public static List<Person> allPeople = new ArrayList<>();

    static {
        allPeople.add(Person.createMale("Donald Chump", new Date()));  // id=0
        allPeople.add(Person.createMale("Larry Gates", new Date()));  // id=1
    }

    public static void main(String[] args) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("MM dd yyyy", Locale.ENGLISH);
        int index;
        String name = "";
        Sex sex;
        Date date;

        if (args[0].equals("-c")) {
            name = args[1];
            sex = args[2].equals("m") ? Sex.MALE : Sex.FEMALE;
            date = dateFormat.parse(args[3]);
            if (sex.equals(Sex.MALE)) {
                allPeople.add(Person.createMale(name, date));
            } else if (sex.equals(Sex.FEMALE)) {
                allPeople.add(Person.createFemale(name, date));
            }
            System.out.println(allPeople.size() - 1);
        } else if (args[0].equals("-u")) {
            index = Integer.parseInt(args[1]);
            name = args[2];
            sex = args[3].equals("m") ? Sex.MALE : Sex.FEMALE;
            date = dateFormat.parse(args[4]);
            if (sex.equals(Sex.MALE)) {
                allPeople.set(index, Person.createMale(name, date));
            } else if (sex.equals(Sex.FEMALE)) {
                allPeople.set(index, Person.createFemale(name, date));
            }
            System.out.println("Successfully uploaded: " + index + " " + name + " " + sex.toString() + " " + date.toString());
        } else if (args[0].equals("-d")) {
            index = Integer.parseInt(args[1]);
            allPeople.get(index).setName(null);
            allPeople.get(index).setSex(null);
            allPeople.get(index).setBirthDate(null);
        } else if (args[0].equals("-i")) {
            dateFormat = new SimpleDateFormat("MMM dd yyyy", Locale.ENGLISH);
            index = Integer.parseInt(args[1]);
            name = allPeople.get(index).getName();
            sex = allPeople.get(index).getSex();
            Date birth = allPeople.get(index).getBirthDate();
            System.out.println(name + " " + sex.toString() + " " + dateFormat.format(birth));

        }
    }
}
