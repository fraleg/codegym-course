package com.codegym.task.task17.task1711;

public enum Sex {
    MALE,
    FEMALE;

    public String toString() {
        if (this.equals(Sex.MALE)){
            return "m";
        } else {
            return "f";
        }
    }
}
