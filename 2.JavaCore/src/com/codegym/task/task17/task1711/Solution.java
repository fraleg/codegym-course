package com.codegym.task.task17.task1711;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/* 
CRUD 2

*/

public class Solution {
    public static volatile List<Person> allPeople = new ArrayList<>();

    static {
        allPeople.add(Person.createMale("Donald Chump", new Date()));  // id=0
        allPeople.add(Person.createMale("Larry Gates", new Date()));  // id=1
//        allPeople.add(Person.createMale("Larry Gates", new Date()));  // id=1
//        allPeople.add(Person.createMale("Larry Gates", new Date()));  // id=1
    }

    public static void main(String[] args) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("MM dd yyyy", Locale.ENGLISH);
        int index;
        String name = "";
        Sex sex;
        Date date;

        switch (args[0]) {
            case "-c": {
                synchronized (allPeople) {
                    for (int i = 1; i < args.length - 2; i = i + 3) {
                        name = args[i];
                        sex = args[i + 1].equals("m") ? Sex.MALE : Sex.FEMALE;
                        date = dateFormat.parse(args[i + 2]);
                        if (sex.equals(Sex.MALE)) {
                            allPeople.add(Person.createMale(name, date));
                        } else if (sex.equals(Sex.FEMALE)) {
                            allPeople.add(Person.createFemale(name, date));
                        }
                        System.out.println(allPeople.size() - 1);
                    }
                }
                break;
            }
            case "-u": {
                synchronized (allPeople) {
                    for (int i = 1; i < args.length - 2; i = i + 4) {
                        index = Integer.parseInt(args[i]);
                        name = args[i + 1];
                        sex = args[i + 2].equals("m") ? Sex.MALE : Sex.FEMALE;
                        date = dateFormat.parse(args[i + 3]);
                        if (sex.equals(Sex.MALE)) {
                            allPeople.set(index, Person.createMale(name, date));
                        } else if (sex.equals(Sex.FEMALE)) {
                            allPeople.set(index, Person.createFemale(name, date));
                        }
                        System.out.println("Successfully uploaded: " + index + " " + name + " " + sex.toString() + " " + date.toString());
                    }
                }
                break;
            }
            case "-d": {
                synchronized (allPeople) {
                    for (int i = 1; i < args.length; i++) {
                        index = Integer.parseInt(args[i]);
                        allPeople.get(index).setName(null);
                        allPeople.get(index).setSex(null);
                        allPeople.get(index).setBirthDate(null);
                    }
                }
                break;
            }
            case "-i": {
                synchronized (allPeople) {
                    for (int i = 1; i < args.length; i++) {
                        dateFormat = new SimpleDateFormat("MMM dd yyyy", Locale.ENGLISH);
                        index = Integer.parseInt(args[i]);
                        name = allPeople.get(index).getName();
                        sex = allPeople.get(index).getSex();
                        Date birth = allPeople.get(index).getBirthDate();
                        System.out.println(name + " " + sex.toString() + " " + dateFormat.format(birth));
                    }
                }
                break;
            }
        }
    }
}
