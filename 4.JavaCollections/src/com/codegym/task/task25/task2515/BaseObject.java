package com.codegym.task.task25.task2515;

public abstract class BaseObject {

    private double x;
    private double y;
    private double radius;
    private boolean isAlive;

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getRadius() {
        return radius;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void draw() {

    }

    public void move() {

    }

    public void die() {
        this.isAlive = false;
    }

    public boolean intersects(BaseObject o) {
        double distance = Math.sqrt(Math.pow(this.x - o.x, 2) + Math.pow(this.y - o.y, 2));
        double max = 0.0;
        if (this.radius >= o.radius) {
            max = this.radius;
        } else {
            max = o.radius;
        }

        if (distance <= max) {
            return true;
        } else {
            return false;
        }
    }

    public BaseObject(double x, double y, double radius) {
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.isAlive = true;
    }
}
