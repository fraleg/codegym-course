package com.codegym.task.task35.task3507;

import com.codegym.task.task35.task3507.data.Cat;
import com.codegym.task.task35.task3507.data.Elephant;
import com.codegym.task.task35.task3507.data.Sheep;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

/* 
What is ClassLoader?

*/
public class Solution {
    public static void main(String[] args) throws InstantiationException, IllegalAccessException {
        Set<? extends Animal> allAnimals = getAllAnimals(Solution.class.getProtectionDomain().getCodeSource().getLocation().getPath() + Solution.class.getPackage().getName().replaceAll("[.]", "/") + "/data");
        System.out.println(allAnimals);
    }

    public static Set<? extends Animal> getAllAnimals(String pathToAnimals) throws IllegalAccessException, InstantiationException {
        Set<Animal> zbior = new HashSet<>();

        try {
            Cat cat = Cat.class.newInstance();
            if (cat instanceof Animal) {
                zbior.add(cat);
            }
        } catch (Exception ex) {
            System.out.println("Cat exception caught");
        }
        try {
            Elephant elephant = Elephant.class.newInstance();
            if (elephant instanceof Animal) {
                //zbior.add(elephant);
            }
        } catch (Exception ex) {
            System.out.println("Elephant exception caught");
        }
        try {
            Sheep sheep = Sheep.class.newInstance();
            if (sheep instanceof Animal) {
                zbior.add(sheep);
            }
        } catch (Exception ex) {
            System.out.println("Sheep exception caught");
        }


        return zbior;
    }
}
