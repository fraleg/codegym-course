package com.codegym.task.task35.task3501;

public class StaticGeneric {
    public static Object someStaticMethod(Object genericObject) {
        System.out.println(genericObject);
        return genericObject;
    }
}