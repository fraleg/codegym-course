package com.codegym.task.task35.task3512;

public class Generator<T> {

    public Class<T> field;

    public Generator(Class<T> field) {
        this.field = field;
    }

    T newInstance() throws IllegalAccessException, InstantiationException {
        return field.newInstance();
    }
}
