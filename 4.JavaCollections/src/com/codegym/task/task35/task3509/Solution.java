package com.codegym.task.task35.task3509;

import java.util.*;


/* 
Collections & Generics

*/
public class Solution {

    public static void main(String[] args) {
    }

    public static <T extends Object> ArrayList newArrayList(T... elements) {
        ArrayList<T> list = new ArrayList<>();
        for (T t : elements) {
            list.add(t);
        }
        return list;
    }

    public static <T> HashSet newHashSet(Object... elements) {
        HashSet<Object> set = new HashSet<>();
        for (Object obj : elements) {
            set.add(obj);
        }
        return set;
    }

    public static <K extends List, V extends List> HashMap newHashMap(List keys, List values) {
        //write your code here
        return null;
    }
}
