package com.codegym.task.task37.task3707;

import java.io.Serializable;
import java.util.*;

public class  AmigoSet<E> extends AbstractSet implements Serializable, Cloneable, Set {

    private static final Object PRESENT = new Object();
    private transient HashMap<E, Object> map;

    @Override
    public Iterator iterator() {
        return null;
    }

    @Override
    public int size() {
        return 0;
    }

    public AmigoSet() {
        map = new HashMap<>();
    }

    public AmigoSet(Collection<? extends E> collection) {
        map = new HashMap<>();
        for (E e : collection) {
            map.put(e, PRESENT);
        }

    }

//    public boolean add(E e) {
//        if (map.containsKey(e)) {
//            return false;
//        } else {
//            map.put(e, PRESENT);
//            return true;
//        }
//    }

}
