package com.codegym.task.task31.task3105;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/* 
Adding a file to an archive

*/
public class Solution {
    public static void main(String[] args) throws IOException {

        // stream for reading from zip
        FileInputStream fis = new FileInputStream(args[1]);
        ZipInputStream zis = new ZipInputStream(fis);
        ArrayList<ZipEntry> list = new ArrayList<>();

        // getting only file name from path
        File f = new File(args[0]);
        String fileName = f.getName();

        // reading file content from zip
        ZipEntry ze;
        while ((ze = zis.getNextEntry()) != null) {
            list.add(ze);
        }
        zis.close();
        fis.close();


        // stream for writing to zip
        FileOutputStream fos = new FileOutputStream(args[1]);
        ZipOutputStream zos = new ZipOutputStream(fos);

        // Put a ZipEntry into it, here there will be only empty file !!!
        zos.putNextEntry(new ZipEntry("new\\" + fileName));
        for (ZipEntry z : list) {
            zos.putNextEntry(z);
        }

        // Copy the file «document-for-archive.txt» to the archive under the name «document.txt»
        File file = new File(args[0]);
        Files.copy(file.toPath(), zos);

        // Close the archive
        zos.close();
        fos.close();

    }
}
