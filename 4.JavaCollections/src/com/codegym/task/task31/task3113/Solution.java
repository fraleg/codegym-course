package com.codegym.task.task31.task3113;

import java.io.BufferedReader;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.stream.Stream;

/* 
What's in the folder?

*/
public class Solution {

    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        String directoryPath = sc.nextLine();
        //String directoryPath = "C:\\Users\\Franciszek\\Desktop\\rezerwacjamiejsca";
        Path file = Paths.get(directoryPath);
        if (!Files.isDirectory(file)) {
            System.out.println(directoryPath + " is not a folder");
            return;
        }

        System.out.println("Total folders: " + Files.walk(file)
                            .parallel()
                            .filter(p -> p.toFile().isDirectory() && !p.equals(file))
                            .count());

        System.out.println("Total files: " + Files.walk(file)
                .parallel()
                .filter(p -> p.toFile().isFile())
                .count());


        long sizeDir = Files.walk(file)
                        .filter(p -> p.toFile().isFile())
                        .mapToLong(p -> p.toFile().length())
                        .sum();
        System.out.println("Total size: " + sizeDir);
    }
}
