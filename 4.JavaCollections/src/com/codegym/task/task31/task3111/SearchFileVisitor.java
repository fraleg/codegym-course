package com.codegym.task.task31.task3111;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SearchFileVisitor extends SimpleFileVisitor<Path> {

    private String partOfName = null;
    private String partOfContent = null;
    private int minSize = Integer.MIN_VALUE;
    private int maxSize = Integer.MAX_VALUE;
    private List<Path> foundFiles = new ArrayList<>();

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        byte[] content = Files.readAllBytes(file); // File size: content.length
        if (partOfName == null || file.toString().contains(partOfName)
        && (partOfContent == null || fileContains(file, partOfContent))
        && (minSize < 0 || file.toFile().length() < maxSize)
        && (maxSize > 0 || file.toFile().length() > minSize)) {
            foundFiles.add(file);
        }

        return super.visitFile(file, attrs);
    }

    private static boolean fileContains(Path file, String partOfContent) throws IOException {
        try (Scanner sc = new Scanner(file)) {

            while (sc.hasNext()) {
                String line = sc.nextLine();
                if (line.contains(partOfContent)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void setPartOfName(String partOfName) {
        this.partOfName = partOfName;
    }

    public void setPartOfContent(String partOfContent) {
        this.partOfContent = partOfContent;
    }

    public void setMinSize(int minSize) {
        this.minSize = minSize;
    }

    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }

    public List<Path> getFoundFiles() {
        return foundFiles;
    }
}
