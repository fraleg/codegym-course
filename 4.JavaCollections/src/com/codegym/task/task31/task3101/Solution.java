package com.codegym.task.task31.task3101;

import java.io.*;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/*
Iterating through a file tree

*/
public class Solution {
    public static void main(String[] args) throws IOException {
        File folder = new File(args[0]);
        LinkedList<String> list = new LinkedList<>();
        Map<String, String> map = new HashMap<>();
        String outputPath = args[1];
        File outputFile = new File(outputPath);
        String dir = outputFile.getParent();
        File newOutputFile = new File(dir + "\\allFilesContent.txt");
        FileUtils.renameFile(outputFile, newOutputFile);
        FileOutputStream fos = new FileOutputStream(newOutputFile);

        for (File file : folder.listFiles()) {
            if (file.isFile()) {
                if (file.length() <= 50) {
                    list.add(file.getName());
                    map.put(file.getName(), file.getAbsolutePath());
                }
            } else {
                for (File f : file.listFiles()) {
                    if (f.isFile()) {
                        if (f.length() <= 50) {
                            list.add(f.getName());
                            map.put(f.getName(), f.getAbsolutePath());
                        }
                    }
                }
            }
        }

        Collections.sort(list);
//        for (String s : list) {
//            System.out.println(s);
//        }



        for (String s : list){
//            FileInputStream fis = new FileInputStream(map.get(s));
//            while (fis.available() > 0) {
//                int data = fis.read();
//                fos.write(data);
//            }
//            fis.close();
//            fos.write(10);
            fos.write(s.getBytes());
            fos.write("\n".getBytes());
        }
        fos.close();
    }
}