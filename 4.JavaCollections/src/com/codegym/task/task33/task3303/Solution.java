package com.codegym.task.task33.task3303;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;

/* 
Deserializing a JSON object

*/
public class Solution {
    public static <T> T convertFromJsonToNormal(String fileName, Class<T> clazz) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        StringBuilder sb = new StringBuilder();
        BufferedReader br = new BufferedReader(new FileReader(fileName));
        String line = null;
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        br.close();

        return mapper.readValue(sb.toString(), clazz);
    }

    public static void main(String[] args) throws IOException {
//        Cat cat = convertFromJsonToNormal("C:\\Users\\Franciszek\\Desktop\\json.txt", Cat.class);
//        System.out.println(cat.name);
//        System.out.println(cat.age);
//        System.out.println(cat.weight);
    }

    @JsonAutoDetect
    public static class Cat {
        @JsonProperty
        public String name;
        @JsonProperty
        public int age;
        @JsonProperty
        public int weight;

        Cat(){}
    }
}