package com.codegym.task.task32.task3201;

import java.io.IOException;
import java.io.RandomAccessFile;

/*
Writing to an existing file

*/
public class Solution {
    public static void main(String... args) throws IOException {

        RandomAccessFile raf = new RandomAccessFile(args[0], "rw");
        int move = Integer.parseInt(args[1]);

        if (raf.length() < move) {
            raf.seek(raf.length());
        } else {
            raf.seek(move);
        }

        for(int i = 0; i < args[2].length(); i++) {
            raf.write(args[2].charAt(i));
        }

        raf.close();
    }
}
