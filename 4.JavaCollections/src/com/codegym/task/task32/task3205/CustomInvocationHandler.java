package com.codegym.task.task32.task3205;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class CustomInvocationHandler implements InvocationHandler {

    private Object original;

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        try {
            System.out.println(String.format("%s in", method.getName()));
            return method.invoke(original, args);
        } finally {
            System.out.println(String.format("%s out", method.getName()));
        }
    }

    public CustomInvocationHandler(SomeInterfaceWithMethods param) {
        this.original = param;
    }
}
