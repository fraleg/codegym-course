package com.codegym.task.task32.task3213;

import java.io.IOException;
import java.io.StringReader;

/* 
Caesar cipher

*/

public class Solution {
    public static void main(String[] args) throws IOException {
        //StringReader reader = new StringReader("Khoor#Dpljr#&C,₷B'3");
        StringReader reader = new StringReader("D E F G H I J K L M N");
        System.out.println(decode(reader, -68));  // Hello Amigo #@)₴?$0
    }

    public static String decode(StringReader reader, int key) throws IOException {
        int valueOfChar;
        String s = "";
        String target = "";
        while ((valueOfChar = reader.read()) != -1) {
            s += (char) valueOfChar;
        }
        reader.close();
        for (int i = 0; i < s.length(); i++) {
            //System.out.print((char)(s.charAt(i) + key));

            target += (char)(s.charAt(i) + key);
        }

        return target;
    }
}
