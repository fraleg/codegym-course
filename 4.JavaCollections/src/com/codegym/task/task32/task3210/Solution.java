package com.codegym.task.task32.task3210;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

/* 
Using RandomAccessFile

*/

public class Solution {
    public static void main(String... args) throws IOException {
        String filePath = args[0];
        int move = Integer.parseInt(args[1]);
        String word = args[2];
        RandomAccessFile raf = new RandomAccessFile(filePath, "rw");
        byte[] buffer = new byte[100];


        raf.seek(move);
        raf.read(buffer, move, (int)raf.length() - move);
        String s = new String(buffer, StandardCharsets.UTF_8);

//        System.out.println(s);
//        System.out.println(word);

        if (s.equals(word)) {
            String answer = "true";
            byte[] b = answer.getBytes();
            raf.write(b);
        } else {
            String answer = "false";
            byte[] b = answer.getBytes();
            raf.write(b);
        }

        raf.close();
    }
}