package com.codegym.task.task32.task3202;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

/* 
Reading from a stream

*/
public class Solution {
    public static void main(String[] args) throws IOException {
        StringWriter writer = getAllDataFromInputStream(new FileInputStream("C:\\Users\\Franciszek\\Desktop\\audi.txt"));
        System.out.println(writer.toString());
    }

    public static StringWriter getAllDataFromInputStream(InputStream is) throws IOException {

        StringWriter writer = new StringWriter();
        while (is.available() > 0) {
            int data = is.read();
            writer.write(data);
        }
        //System.out.println(writer);
        if (writer != null) {
            return writer;
        } else {
            return new StringWriter(1);

        }

    }
}