package com.codegym.task.task34.task3403;

/* 
Factorization using recursion

*/
public class Solution {

    public void recurse(int n) {
        recurse(n, 2);
    }

    public void recurse(int n, int f) {
        if (n == 1) return;

        if (n % f == 0) {
            System.out.print(f + " ");
            recurse(n / f, f);
        } else {
            recurse(n, f + 1);
        }
    }
}