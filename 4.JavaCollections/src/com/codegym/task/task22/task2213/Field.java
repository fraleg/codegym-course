package com.codegym.task.task22.task2213;

import static java.lang.Thread.sleep;

public class Field {

    private int width;
    private int height;
    private int[][] matrix;

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int[][] getMatrix() {
        return matrix;
    }

    public void print() throws InterruptedException {
        for (int i = 0; i < height; i++) {
            System.out.println();
            for (int j = 0; j < width; j++) {
                if (i == 0 || j == 0 || i == height - 1 || j == height - 1) {
                    //System.out.print(".");
                }
            }

        }
        sleep(5000);
    }

    public void removeFullLines() {

    }

    public Integer getValue(int x, int y) {
        return matrix[x][y];
    }

    public void setValue(int x, int y, int value) {
        matrix[x][y] = value;
    }

    public Field(int width, int height) {
        this.width = width;
        this.height = height;
        this.matrix = new int[height][width];
    }
}
