package com.codegym.task.task22.task2201;

public class OurUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {
    @Override
    public void uncaughtException(Thread t, Throwable e) {
        final String string = "%s : %s : %s";
        if (Solution.FIRST_THREAD_NAME.equals(t.getName())) {
            System.out.println(getFormattedStringForFirstThread(t, e, string));
        } else if (Solution.SECOND_THREAD_NAME.equals(t.getName())) {
            System.out.println(getFormattedStringForSecondThread(t, e, string));
        } else {
            System.out.println(getFormattedStringForOtherThread(t, e, string));
        }
    }

    protected String getFormattedStringForSecondThread(Thread t, Throwable e, String string) {
        int start = e.getClass().toString().indexOf("String");
        String s = String.format(string, e.getLocalizedMessage(), e.getClass().toString().substring(start), t.getName());

        return s;
    }

    protected String getFormattedStringForFirstThread(Thread t, Throwable e, String string) {
        int end = e.toString().indexOf(e.getMessage()) - 2;
        int start = e.toString().indexOf("String");
        System.out.println(start);
        String s = String.format(string, t.getName(), e.toString().substring(start, end), e.getMessage());

        return s;
    }

    protected String getFormattedStringForOtherThread(Thread t, Throwable e, String string) {
        int i = e.toString().indexOf(e.getMessage());
        String s = String.format(string, e.toString().substring(10, i - 2), e.getMessage(), t.getName());

        return s;
    }
}
