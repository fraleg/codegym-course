package com.codegym.task.task22.task2207;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/* 
Inverted words

*/
public class Solution {
    public static List<Pair> result = new LinkedList<>();

    public static void main(String[] args) throws IOException {
        //String fileName = "C:\\Users\\Franciszek\\Desktop\\file.txt";
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = reader.readLine();
        reader.close();

        BufferedReader br = new BufferedReader(new FileReader(fileName));
        StringBuilder sb = new StringBuilder();
        String line = null;

        while ((line = br.readLine()) != null) {
            sb.append(line);
            sb.append(" ");
        }
        br.close();

        String[] tab = sb.toString().split(" ");

        for (int i = 0; i < tab.length; i++) {
//            for (String s : tab) {
//                System.out.print(s + " ");
//            }
//            System.out.println("NOWY OBIEG PĘTLI");
            if (tab[i] == null){
                continue;
            } else {
                String firstWord = tab[i];

                for (int j = 0; j < tab.length; j++) {
                    if (j == i || tab[j] == null) {
                        continue;
                    } else {
                        StringBuilder secondWord = new StringBuilder();
                        secondWord.append(tab[j]);
                        String reverseWord = secondWord.reverse().toString();

                        if (firstWord.equals(reverseWord)) {
                            Pair pair = new Pair();
                            pair.first = firstWord;
                            pair.second = secondWord.reverse().toString();
                            if (!result.contains(pair)) {
                                result.add(pair);
                                tab[i] = null;
                                tab[j] = null;
                            }
                        }

                    }
                }
            }
        }

        for (Pair p : result) {
            System.out.println(p.first + " " + p.second);
        }
    }

    public static class Pair {
        String first;
        String second;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Pair pair = (Pair) o;

            if (first != null ? !first.equals(pair.first) : pair.first != null) return false;
            return second != null ? second.equals(pair.second) : pair.second == null;

        }

        @Override
        public int hashCode() {
            int result = first != null ? first.hashCode() : 0;
            result = 31 * result + (second != null ? second.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return first == null && second == null ? "" :
                    first == null ? second :
                            second == null ? first :
                                    first.compareTo(second) < 0 ? first + " " + second : second + " " + first;
        }
    }

}