package com.codegym.task.task22.task2202;

/* 
Find a substring

*/
public class Solution {
    public static void main(String[] args) {
        System.out.println(getPartOfString("CodeGym is the best place to learn Java."));
        //System.out.println(getPartOfString("hahahah"));
    }

    public static String getPartOfString(String string) throws StringTooShortException {
//        try {
//            if (string == null) throw new StringTooShortException();
//            StringBuilder sb = new StringBuilder();
//            int firstSpace = string.indexOf(" ");
//            string = string.substring(firstSpace + 1);
//
//            int secondSpace = string.indexOf(" ");
//            sb.append(string.substring(0, secondSpace + 1));
//            string = string.substring(secondSpace + 1);
//
//            int thirdSpace = string.indexOf(" ");
//            sb.append(string.substring(0, thirdSpace + 1));
//            string = string.substring(thirdSpace + 1);
//
//            int fourthSpace = string.indexOf(" ");
//            sb.append(string.substring(0, fourthSpace + 1));
//            string = string.substring(fourthSpace + 1);
//
//            int fifthSpace = string.indexOf(" ");
//            sb.append(string.substring(0, fifthSpace));
//
//            return sb.toString();
//        } catch (StringIndexOutOfBoundsException ex) {
//            throw new StringTooShortException();
//        }
        if (string == null) throw new StringTooShortException();

        StringBuilder sb = new StringBuilder();
        String[] tab = string.split(" ");
        if (tab.length < 5) throw new StringTooShortException();
        for (int i = 1; i < 5; i++) {
            sb.append(tab[i]);
            sb.append(" ");
        }
        return sb.toString().substring(0, sb.toString().length() - 1);

    }

    public static class StringTooShortException extends RuntimeException {
    }
}