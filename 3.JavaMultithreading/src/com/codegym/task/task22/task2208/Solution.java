package com.codegym.task.task22.task2208;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/* 
Build a WHERE query

*/
public class Solution {
    public static void main(String[] args) {
        LinkedHashMap<String, String> map = new LinkedHashMap<>();
        map.put("name", null);
        map.put("country", null);
        map.put("city", null);
        map.put("age", null);
        System.out.println(getQuery(map));
    }
    public static String getQuery(Map<String, String> params) {
        Iterator<Map.Entry<String, String>> it = params.entrySet().iterator();
        StringBuilder sb = new StringBuilder();

        while (it.hasNext()) {
            Map.Entry<String, String> entry = it.next();
            if (entry.getValue() != null) {
                sb.append(entry.getKey() + " = '" + entry.getValue() + "' and ");
            }
        }
        if (!sb.toString().equals("")){
            int index = sb.toString().lastIndexOf(" and ");
            return sb.toString().substring(0, index);
        } else {
            return "";
        }

    }
}
