package com.codegym.task.task22.task2209;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;

/*
Make a word chain

*/
public class Solution {
    public static void main(String[] args) throws IOException {

        String filename = "C:\\Users\\Franciszek\\Desktop\\nowy.txt";
        BufferedReader br = new BufferedReader(new FileReader(filename));
        String line = br.readLine();
        String[] tab = line.split(" ");

        StringBuilder result = getLine(tab);
        System.out.println(result.toString());
    }

    public static StringBuilder getLine(String... words) {
        LinkedList<String> list = new LinkedList<>();
        for (int i = 0; i < words.length; i++) {
            list.add(words[i]);
        }

        StringBuilder sb = new StringBuilder();
        sb.append(list.get(0));
        Character beginChain = sb.toString().charAt(0);
        Character endChain = sb.toString().charAt(sb.toString().length() - 1);
        list.remove(0);


        for (int i = 0; i < list.size(); i++) {
            System.out.println(i);
            if (list.get(i).startsWith(endChain.toString().toUpperCase())) {
                sb.append(" " + list.get(i));
                endChain = sb.toString().charAt(sb.toString().length() - 1);
                list.remove(i);
            }

            if (list.get(i).endsWith(beginChain.toString().toLowerCase())) {
                sb.insert(0, list.get(i) + " ");
                beginChain = sb.toString().charAt(0);
                list.remove(i);
            }

            System.out.println(beginChain + " " + endChain);

            for (String s : list) {
                System.out.print(s + "\t");
            }
            System.out.println();
        }

        return sb;
    }
}