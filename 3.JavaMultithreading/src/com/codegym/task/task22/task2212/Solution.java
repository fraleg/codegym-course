package com.codegym.task.task22.task2212;

/* 
Phone number verification

*/
public class Solution {
    public static boolean checkPhoneNumber(String phoneNumber) {
        if (phoneNumber == null) return false;
        int length = phoneNumber.replaceAll("[^0-9]", "").length();

        if (length == 12) {
            // starts with '+'
            if (phoneNumber.matches("^\\+\\d{8}\\-?\\d{2}\\-?\\d{2}$")) return true;
            if (phoneNumber.matches("^\\+\\d{2}\\(\\d{3}\\)\\d{3}\\-?\\d{2}\\-?\\d{2}$")) return true;
            // can have 0 or a few '#'
            // can have 1 or none '(###)'
            //can have 1 or none "-#"
            //can have 1 or none "#-#"
            // return true if match
        } else if (length == 10) {
            if (phoneNumber.matches("^\\(\\d{3}\\)\\d{3}\\-?\\d{2}\\-?\\d{2}$")) return true;
            if (phoneNumber.matches("^\\d{6}\\-?\\d{2}\\-?\\d{2}$")) return true;
            // check for 10 digit number
            // return true if match
        }
        return false;
    }

    public static void main(String[] args) {
//        System.out.println(checkPhoneNumber("+380501234567"));
//        System.out.println(checkPhoneNumber("+38(050)1234567"));
//        System.out.println(checkPhoneNumber("+38(050)123-4567"));
//        System.out.println(checkPhoneNumber("+38(050)123-45-67"));
//        System.out.println(checkPhoneNumber("+38050123-4567"));
//        System.out.println(checkPhoneNumber("+38050123-45-67"));
//        System.out.println(checkPhoneNumber("+38(050)1234567"));
//        System.out.println(checkPhoneNumber("+38(050)123-4567"));
//        System.out.println(checkPhoneNumber("+38(050)123-45-67"));
//        System.out.println(checkPhoneNumber("(050)1234567"));
//        System.out.println(checkPhoneNumber("(050)123-4567"));
//        System.out.println(checkPhoneNumber("(050)123-45-67"));
//        System.out.println(checkPhoneNumber("0501234567"));
//        System.out.println(checkPhoneNumber("050123-4567"));
//        System.out.println(checkPhoneNumber("050123-45-67"));
    }
}
