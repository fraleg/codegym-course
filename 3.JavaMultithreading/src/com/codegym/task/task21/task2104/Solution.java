package com.codegym.task.task21.task2104;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/* 
Equals and HashCode

*/
public class Solution {
    private final String first, last;

    public Solution(String first, String last) {
        this.first = first;
        this.last = last;
    }

    public boolean equals(Object n) {
        if (n == this) {
            return true;
        }
        if (!(n instanceof Solution)) {
            return false;
        }
        Solution s = (Solution) n;
        return Objects.equals(this.first, s.first) && Objects.equals(this.last, s.last);
    }

    public int hashCode() {
        if (first == null && last != null) return 31 * 0 + last.hashCode();
        else if (first != null && last == null) return 31 * first.hashCode() + 0;
        else if (first == null && last == null) return 0;
        else return 31 * first.hashCode() + last.hashCode();
    }

    public static void main(String[] args) {
        Set<Solution> s = new HashSet<>();
        s.add(new Solution("Donald", "Duck"));
        System.out.println(s.contains(new Solution("Donald", "Duck")));
    }
}
