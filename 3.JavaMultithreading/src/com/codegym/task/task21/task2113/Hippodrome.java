package com.codegym.task.task21.task2113;

import java.util.ArrayList;
import java.util.List;

public class Hippodrome {

    public static Hippodrome game;
    private List<Horse> horses;

    public List<Horse> getHorses() {
        return horses;
    }

    public Hippodrome(List<Horse> horses) {
        this.horses = horses;
    }

    public void run() throws InterruptedException {
        for (int i = 1; i <= 100; i++) {
            move();
            print();
            Thread.sleep(200);
        }
    }

    public void move() {
        for (Horse h : horses) {
            h.move();
        }
    }

    public void print() {
        for (Horse h : horses) {
            h.print();
        }
        for (int i = 0; i < 10; i++) {
            System.out.println();
        }
    }

    public Horse getWinner() {
        Horse winner = null;
        double distance = 0.0;

        for (Horse horse : horses) {
            if (horse.getDistance() > distance) {
                winner = horse;
                distance = horse.getDistance();
            }
        }
        return winner;
    }

    public void printWinner() {
        System.out.println("Winner is " + getWinner().getName() + "!");
    }

    public static void main(String[] args) throws InterruptedException {
        List<Horse> lista = new ArrayList<>();
        game = new Hippodrome(lista);

        Horse mustang = new Horse("Mustang", 3, 0);
        Horse amanda = new Horse("Amanda", 3, 0);
        Horse kali = new Horse("Kali", 3, 0);

        game.horses.add(mustang);
        game.horses.add(amanda);
        game.horses.add(kali);

        game.run();
        game.printWinner();
    }
}
