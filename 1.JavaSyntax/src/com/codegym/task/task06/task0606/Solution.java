package com.codegym.task.task06.task0606;

import java.io.*;
import java.util.Scanner;

/* 
Even and odd digits

*/

public class Solution {

    public static int even;
    public static int odd;

    public static void main(String[] args) throws IOException {

        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();;
        int digit;


        while(number > 0){
            digit = number % 10;
            number /= 10;
            if(digit % 2 == 0){
                even++;
            } else {
                odd++;
            }
        }
        System.out.println("Even: " + even + " Odd: " + odd);
    }
}
