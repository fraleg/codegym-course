package com.codegym.task.task06.task0610;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

/* 
ConsoleReader class

*/

public class ConsoleReader {

    public static String readString() throws Exception {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        return str;
    }

    public static int readInt() throws Exception {
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();
        return number;
    }

    public static double readDouble() throws Exception {
        Scanner sc = new Scanner(System.in);
        double numberDouble = sc.nextDouble();
        return numberDouble;
    }

    public static boolean readBoolean() throws Exception {
        Scanner sc = new Scanner(System.in);
        Boolean str = sc.nextBoolean();
        if(str){
            return true;
        } else {
            return false;
        }
    }

    public static void main(String[] args) {

    }
}
