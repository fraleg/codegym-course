package com.codegym.task.task07.task0712;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

/* 
Shortest or longest

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        ArrayList<String> list = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        int lenghtShortest = 2147483647;
        int lenghtLongest = -2147483647;

        for(int i = 0; i < 10; i++){
            String str = sc.nextLine();
            list.add(str);
            if(str.length() < lenghtShortest){
                lenghtShortest = str.length();
            }
            if(str.length() > lenghtLongest){
                lenghtLongest= str.length();
            }
        }

        for(int i = 0; i < list.size(); i++){
            if(list.get(i).length() == lenghtShortest || list.get(i).length() == lenghtLongest){
                System.out.println(list.get(i));
                break;
            }
        }
    }
}
