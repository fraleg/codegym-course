package com.codegym.task.task07.task0721;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Min and max in arrays

*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int[] tab = new int[20];
        int maximum;
        int minimum;

        for (int i = 0; i < tab.length; i++){
            tab[i] = Integer.parseInt(reader.readLine());
        }
        minimum = tab[0];
        maximum = tab[0];
        for (int i = 1; i < tab.length; i++){
            if(tab[i] < minimum){
                minimum = tab[i];
            }
            if(tab[i] > maximum){
                maximum = tab[i];
            }
        }

        System.out.print(maximum + " " + minimum);
    }
}
