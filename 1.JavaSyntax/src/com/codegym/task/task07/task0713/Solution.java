package com.codegym.task.task07.task0713;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/* 
Playing Javarella

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        ArrayList<Integer> list = new ArrayList<Integer>();
        ArrayList<Integer> listDivBy3 = new ArrayList<Integer>();
        ArrayList<Integer> listDivBy2 = new ArrayList<Integer>();
        ArrayList<Integer> listOthers = new ArrayList<Integer>();

        for (int i = 0; i < 20; i++){
            list.add(sc.nextInt());
        }

        for(int i = 0; i < list.size(); i++) {
            if (list.get(i) % 3 == 0)
                listDivBy3.add(list.get(i));
        }

        for(int i = 0; i < list.size(); i++) {
            if (list.get(i) % 2 == 0)
                listDivBy2.add(list.get(i));
        }

        for(int i = 0; i < list.size(); i++) {
            if (list.get(i) % 3 != 0 && list.get(i) % 2 != 0)
                listOthers.add(list.get(i));
        }

        printList(listDivBy3);
        printList(listDivBy2);
        printList(listOthers);
    }

    public static void printList(List<Integer> list) {
        for (int i = 0; i < list.size(); i++){
            System.out.println(list.get(i));
        }
    }
}
