package com.codegym.task.task07.task0705;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

/* 
One large array and two small ones

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        int[] numbers = new int[20];
        int[] copy1 = new int[10];
        int[] copy2 = new int[10];

        for(int i = 0; i < numbers.length; i++){
            numbers[i] = sc.nextInt();
        }

        for(int i = 0; i < copy1.length; i++) {
            copy1[i] = numbers[i];
            copy2[i] = numbers[i + 10];
            System.out.println(copy2[i]);
        }


    }
}
