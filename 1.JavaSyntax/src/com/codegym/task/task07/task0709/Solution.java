package com.codegym.task.task07.task0709;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

/* 
Expressing ourselves more concisely

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        ArrayList<String> strings = new ArrayList<>();
        int strlength = 2147483647;

        for(int i = 0; i < 5; i++){
            String str = sc.nextLine();
            strings.add(str);
            if(strings.get(i).length() < strlength){
                strlength = strings.get(i).length();
            }
        }

        for(int i = 0; i < strings.size(); i++){
            if(strings.get(i).length() == strlength){
                System.out.println(strings.get(i));
            }
        }
    }
}
