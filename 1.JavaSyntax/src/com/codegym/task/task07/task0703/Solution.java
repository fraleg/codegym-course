package com.codegym.task.task07.task0703;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* 
Lonely arrays interact

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String[] str = new String[10];
        int[] num = new int[10];

        for(int i = 0; i < str.length; i++){
            str[i] = br.readLine();
            num[i] = str[i].length();
        }

        for(int i = 0; i < num.length; i++){
            System.out.println(num[i]);
        }
    }
}
