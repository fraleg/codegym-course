package com.codegym.task.task07.task0708;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/* 
Longest string

*/

public class Solution {
    private static List<String> strings = new ArrayList<>();

    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        int strlength = 0;

        for(int i = 0; i < 5; i++){
            String str = sc.nextLine();
            strings.add(str);
            if(strings.get(i).length() > strlength){
                strlength = strings.get(i).length();
            }
        }

        for(int i = 0; i < strings.size(); i++){
            if(strings.get(i).length() == strlength){
                System.out.println(strings.get(i));
            }
        }
    }
}
