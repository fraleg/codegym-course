package com.codegym.task.task07.task0716;

import java.util.ArrayList;
import java.util.ListIterator;

/* 
R or L

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        ArrayList<String> list = new ArrayList<String>();
        list.add("rose"); //0
        list.add("love"); //1
        list.add("lyre"); //2
        list = fix(list);

        for (String s : list) {
            System.out.println(s);
        }
    }

    public static ArrayList<String> fix(ArrayList<String> list) {
        int i = 0;

        String[] arr = new String[list.size()];

        ListIterator<String> it = list.listIterator();

        while (it.hasNext()){
            String s = it.next();
            if (s.contains("r") && !s.contains("l")){
                it.remove();
            } else if (s.contains("l") && !s.contains("r")){
                arr[i] = s;
                i++;
            }
        }

        for (i = 0; arr[i] != null; i++){
            it.add(arr[i]);
        }

        return list;
    }
}