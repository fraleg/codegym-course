package com.codegym.task.task07.task0715;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

/* 
More Sam-I-Am

*/

public class Solution {
    public static void main(String[] args) throws Exception {
       ArrayList<String> list = new ArrayList<>();
       Scanner sc = new Scanner(System.in);

       list.add("Sam");
       list.add("Ham");
       list.add("I");
       list.add("Ham");
       list.add("Am");
       list.add("Ham");

       for (int i = 0; i < list.size(); i++){
           System.out.println(list.get(i));
       }
    }
}
