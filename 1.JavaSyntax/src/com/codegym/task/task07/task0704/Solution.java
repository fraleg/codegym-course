package com.codegym.task.task07.task0704;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

/* 
Flip the array

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        int[] num = new int[10];

        for(int i = 0; i < num.length; i++){
            num[i] = sc.nextInt();
        }

        for(int i = num.length - 1; i >= 0; i--){
            System.out.println(num[i]);
        }
    }
}

