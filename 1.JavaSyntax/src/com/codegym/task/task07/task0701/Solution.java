package com.codegym.task.task07.task0701;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/* 
Maximum in an array

*/

public class Solution {
    public static void main(String[] args) throws Exception {

        int[] array = initializeArray();
        int max = max(array);
        System.out.println(max);
    }

    public static int[] initializeArray() throws IOException {
        Scanner sc = new Scanner(System.in);
        int[] numbers = new int[20];
        for(int i = 0; i < numbers.length; i++){
            numbers[i] = sc.nextInt();
        }
        return numbers;
    }

    public static int max(int[] array) {
        int max = array[0];
        for(int i = 0; i < array.length; i++){
            if(array[i] > max)
                max = array[i];
        }
        return max;
    }
}
