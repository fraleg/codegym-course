package com.codegym.task.task07.task0724;

/* 
Family census

*/

public class Solution {
    public static void main(String[] args) {
        Human grandFather = new Human("Lucjan", true, 81);
        Human grandMother = new Human("Teodozja", false, 77);
        Human grandFather2 = new Human("Kazimierz", true, 74);
        Human grandMother2 = new Human("Anna", false, 71);
        Human father = new Human("Andrzej", true, 62, grandFather, grandMother);
        Human mother = new Human("Grażyna", false, 57, grandFather2, grandMother2);
        Human son1 = new Human("Przemek", true, 36, father, mother);
        Human son2 = new Human("Łukasz", true, 33, father, mother);
        Human son3 = new Human("Franek", true, 24, father, mother);

        System.out.println(grandFather);
        System.out.println(grandMother);
        System.out.println(grandFather2);
        System.out.println(grandMother2);
        System.out.println(father);
        System.out.println(mother);
        System.out.println(son1);
        System.out.println(son2);
        System.out.println(son3);
    }

    public static class Human {

        String name;
        boolean sex;
        int age;
        Human father;
        Human mother;

        public Human(String name, boolean sex, int age){
            this.name = name;
            this.sex = sex;
            this.age = age;
        }

        public Human(String name, boolean sex, int age, Human father, Human mother){
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.father = father;
            this.mother = mother;
        }

        public String toString() {
            String text = "";
            text += "Name: " + this.name;
            text += ", sex: " + (this.sex ? "male" : "female");
            text += ", age: " + this.age;

            if (this.father != null)
                text += ", father: " + this.father.name;

            if (this.mother != null)
                text += ", mother: " + this.mother.name;

            return text;
        }
    }
}