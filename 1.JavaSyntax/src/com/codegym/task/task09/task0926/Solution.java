package com.codegym.task.task09.task0926;

import java.util.ArrayList;

/* 
List of number arrays

*/

public class Solution {
    public static void main(String[] args) {
        ArrayList<int[]> list = createList();
        printList(list);
    }

    public static ArrayList<int[]> createList() {
        ArrayList<int[]> lista = new ArrayList<>();
        int[] tab = {1,2,3,4,5};
        int[] tab2 = {1,2};
        int[] tab3 = {1,2,3,4};
        int[] tab4 = {1,2,3,4,5,6,7};
        int[] tab5 = {};

        lista.add(tab);
        lista.add(tab2);
        lista.add(tab3);
        lista.add(tab4);
        lista.add(tab5);

        return lista;
    }

    public static void printList(ArrayList<int[]> list) {
        for (int[] array : list) {
            for (int x : array) {
                System.out.println(x);
            }
        }
    }
}
