package com.codegym.task.task09.task0927;

import java.util.*;

/* 
Ten cats

*/

public class Solution {
    public static void main(String[] args) {
        Map<String, Cat> map = createMap();
        Set<Cat> set = convertMapToSet(map);
        printCatSet(set);
    }

    public static Map<String, Cat> createMap() {
        Map<String, Cat> map = new HashMap<>();
        Cat cat1 = new Cat("Mruczek");
        Cat cat2 = new Cat("abc");
        Cat cat3 = new Cat("ff");
        Cat cat4 = new Cat("fff");
        Cat cat5 = new Cat("gg");
        Cat cat6 = new Cat("ggg");
        Cat cat7 = new Cat("hh");
        Cat cat8 = new Cat("Mrjjuczek");
        Cat cat9 = new Cat("Mrkkkkkuczek");
        Cat cat10 = new Cat("Mdffddfdfdfdruczek");
        map.put(cat1.name, cat1);
        map.put(cat2.name, cat2);
        map.put(cat3.name, cat3);
        map.put(cat4.name, cat4);
        map.put(cat5.name, cat5);
        map.put(cat6.name, cat6);
        map.put(cat7.name, cat7);
        map.put(cat8.name, cat8);
        map.put(cat9.name, cat9);
        map.put(cat10.name, cat10);

        return map;
    }

    public static Set<Cat> convertMapToSet(Map<String, Cat> map) {
        HashSet<Cat> set = new HashSet<>();
        Iterator<Map.Entry<String, Cat>> itr = map.entrySet().iterator();

        while (itr.hasNext()){
            Map.Entry<String, Cat> pair = itr.next();
            Cat element = pair.getValue();
            set.add(element);
        }

        return set;
    }

    public static void printCatSet(Set<Cat> set) {
        for (Cat cat : set) {
            System.out.println(cat);
        }
    }

    public static class Cat {
        private String name;

        public Cat(String name) {
            this.name = name;
        }

        public String toString() {
            return "Cat " + this.name;
        }
    }


}
