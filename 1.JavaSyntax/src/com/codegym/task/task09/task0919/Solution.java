package com.codegym.task.task09.task0919;

/* 
Dividing by zero

*/

public class Solution {

    public static void main(String[] args) {
        try {
            divideByZero();
        } catch (Exception ex) {
            //System.out.println("złapano wyjątek");
            ex.printStackTrace();
        }
    }

    public static void divideByZero() {
        System.out.println(5 / 0);
    }
}
