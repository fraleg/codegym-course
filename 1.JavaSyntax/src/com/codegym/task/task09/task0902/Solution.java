package com.codegym.task.task09.task0902;

/* 
Stack trace revisited

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        method1();
    }

    public static String method1() {
        method2();
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        System.out.println("method1: " + stackTraceElements[2].getMethodName());
        for (StackTraceElement element : stackTraceElements)
        {
            System.out.print(element.getMethodName() + " ");
        }
        System.out.println();
        return Thread.currentThread().getStackTrace()[2].getMethodName();
    }

    public static String method2() {
        method3();
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        System.out.println("method2: " + stackTraceElements[2].getMethodName());
        for (StackTraceElement element : stackTraceElements)
        {
            System.out.print(element.getMethodName() + " ");
        }
        System.out.println();
        return Thread.currentThread().getStackTrace()[2].getMethodName();
    }

    public static String method3() {
        method4();
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        System.out.println("method3: " + stackTraceElements[2].getMethodName());
        for (StackTraceElement element : stackTraceElements)
        {
            System.out.print(element.getMethodName() + " ");
        }
        System.out.println();
        return Thread.currentThread().getStackTrace()[2].getMethodName();
    }

    public static String method4() {
        method5();
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        System.out.println("method4: " + stackTraceElements[2].getMethodName());
        for (StackTraceElement element : stackTraceElements)
        {
            System.out.print(element.getMethodName() + " ");
        }
        System.out.println();
        return Thread.currentThread().getStackTrace()[2].getMethodName();
    }

    public static String method5() {
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        System.out.println("method5: " + stackTraceElements[2].getMethodName());
        for (StackTraceElement element : stackTraceElements)
        {
            System.out.print(element.getMethodName() + " ");
        }
        System.out.println();
        return Thread.currentThread().getStackTrace()[2].getMethodName();
    }
}
