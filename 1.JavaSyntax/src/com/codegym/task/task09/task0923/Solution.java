package com.codegym.task.task09.task0923;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

/* 
Vowels and consonants

*/

public class Solution {
    public static char[] vowels = new char[]{'a', 'e', 'i', 'o', 'u'};

    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        ArrayList<Character> vowelLetters = new ArrayList<>();
        ArrayList<Character> notVowelLetters = new ArrayList<>();
        String str = sc.nextLine();

        for (int i = 0; i < str.length(); i++){
            if (isVowel(str.charAt(i))){
                vowelLetters.add(str.charAt(i));
            }
            else if (str.charAt(i) == ' '){
                continue;
            }
            else {
                notVowelLetters.add(str.charAt(i));
            }
        }

        for (int i = 0; i < vowelLetters.size(); i++){
            System.out.print(vowelLetters.get(i) + " ");
        }
        System.out.println();
        for (int i = 0; i < notVowelLetters.size(); i++){
            System.out.print(notVowelLetters.get(i) + " ");
        }
        System.out.println();
    }

    // The method checks whether a letter is a vowel
    public static boolean isVowel(char c) {
        c = Character.toLowerCase(c);  // Convert to lowercase

        for (char d : vowels)   // Look for vowels in the array
        {
            if (c == d)
                return true;
        }
        return false;
    }
}