package com.codegym.task.task08.task0828;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/* 
Month number

*/

public class Solution {
    public static void main(String[] args) throws IOException {
        HashMap<String, Integer> months = new HashMap<String, Integer>();
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        months.put("January", 1);
        months.put("February", 2);
        months.put("March", 3);
        months.put("April", 4);
        months.put("May", 5);
        months.put("June", 6);
        months.put("July", 7);
        months.put("August", 8);
        months.put("September", 9);
        months.put("October", 10);
        months.put("November", 11);
        months.put("December", 12);

        String month = br.readLine();
        month = month.substring(0,1).toUpperCase() + month.substring(1).toLowerCase();
        System.out.println(month + " is month " + months.get(month));
//        Iterator<Map.Entry<String, Integer>> iterator = months.entrySet().iterator();
//
//        while (iterator.hasNext()){
//            Map.Entry<String, Integer> pair = iterator.next();
//            String key = pair.getKey();
//            Integer value = pair.getValue();
//            if (month.equals(key)){
//                System.out.println(key + " is month " + value);
//            }
//        }

    }
}
