package com.codegym.task.task08.task0827;

import javafx.scene.input.DataFormat;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/* 
Working with dates

*/

public class Solution {
    public static void main(String[] args) throws ParseException {
        System.out.println(isDateOdd("FEBRUARY 1 2013"));
    }

    public static boolean isDateOdd(String date) throws ParseException {
        Date dat = new Date(date);
        Calendar cal = Calendar.getInstance();
        cal.setTime(dat);
        int days = cal.get(Calendar.DAY_OF_YEAR);
        if(days % 2 == 1){
            return true;
        } else {
            return false;
        }
    }
}
