package com.codegym.task.task08.task0819;


import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/* 
Set of cats

*/

public class Solution {
    public static void main(String[] args) {
        Set<Cat> cats = createCats();

        Iterator<Cat> itr = cats.iterator();

        while (itr.hasNext()){
            Cat cat = itr.next();
            itr.remove();
            break;
        }

        printCats(cats);
    }

    public static Set<Cat> createCats() {
        Set<Cat> set = new HashSet<>();

        set.add(new Cat("Frodo"));
        set.add(new Cat("Maciej"));
        set.add(new Cat("Kicius"));
        return set;
    }

    public static void printCats(Set<Cat> cats) {
        Iterator<Cat> it = cats.iterator();

        while (it.hasNext()){
            Cat cat = it.next();
            System.out.println(cat);
        }
    }

    public static class Cat{

        String name;

        public Cat(){
            this.name = name;
        }

        public Cat(String name){
            this.name = name;
        }
    }
}
