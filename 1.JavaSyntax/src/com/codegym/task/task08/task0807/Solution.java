package com.codegym.task.task08.task0807;

import java.util.*;

/* 
LinkedList and ArrayList

*/

public class Solution {
    public static Object createArrayList() {
        return new ArrayList<Integer>();
    }

    public static Object createLinkedList() {
        return new LinkedList<Integer>();
    }

    public static void main(String[] args) {

    }
}
