package com.codegym.task.task08.task0815;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/* 
Census

*/

public class Solution {
    public static HashMap<String, String> createMap() {
        HashMap<String, String> map = new HashMap<String, String>();

        for (int i = 0; i < 10; i++){
            map.put(Integer.toString(i), Integer.toString(i));
        }
        return map;
    }

    public static int getSameFirstNameCount(HashMap<String, String> map, String name) {
        int firstNameCount = 0;

        for (Map.Entry<String, String> namePair : map.entrySet()){
            if (namePair.getValue().equalsIgnoreCase(name)){
                firstNameCount++;
            }
        }
        return firstNameCount;
    }

    public static int getSameLastNameCount(HashMap<String, String> map, String lastName) {
        int lastNameCount = 0;

        for (Map.Entry<String, String> namePair : map.entrySet()){
            if (namePair.getKey().equalsIgnoreCase(lastName)){
                lastNameCount++;
            }
        }
        return lastNameCount;
    }

    public static void main(String[] args) {

    }
}
