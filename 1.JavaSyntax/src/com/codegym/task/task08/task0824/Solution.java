package com.codegym.task.task08.task0824;

/* 
Make a family

*/

import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) {
        Human child1 = new Human("Beata", false, 13, new ArrayList<Human>());
        Human child2 = new Human("Krzysiek", true, 18, new ArrayList<Human>());
        Human child3 = new Human("Maciek", true, 23, new ArrayList<Human>());

        ArrayList<Human> children = new ArrayList<>();
        children.add(child1);
        children.add(child2);
        children.add(child3);

        Human father = new Human("Franek", true, 37, children);
        Human mother = new Human("Oliwia", false, 35, children);

        ArrayList<Human> fathers = new ArrayList<>();
        fathers.add(father);

        ArrayList<Human> mothers = new ArrayList<>();
        mothers.add(mother);

        Human grandFather = new Human("Kazimierz",true, 79, fathers);
        Human grandMother = new Human("Anna", false, 69, fathers);

        Human grandFather2 = new Human("Lucjan",true, 79, mothers);
        Human grandMother2 = new Human("Teodozja", false, 69, mothers);

        System.out.println(grandFather.toString());
        System.out.println(grandMother.toString());
        System.out.println(grandFather2.toString());
        System.out.println(grandMother2.toString());
        System.out.println(father.toString());
        System.out.println(mother.toString());
        System.out.println(child1.toString());
        System.out.println(child2.toString());
        System.out.println(child3.toString());
    }

    public static class Human {

        String name;
        boolean sex;
        int age;
        ArrayList<Human> children;

//        public Human(String name, boolean sex, int age){
//            this.name = name;
//            this.sex = sex;
//            this.age = age;
//        }

        public Human(String name, boolean sex, int age, ArrayList<Human> children){
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.children = children;
        }

        public String toString() {
            String text = "";
            text += "Name: " + this.name;
            text += ", sex: " + (this.sex ? "male" : "female");
            text += ", age: " + this.age;

            int childCount = this.children.size();
            if (childCount > 0) {
                text += ", children: " + this.children.get(0).name;

                for (int i = 1; i < childCount; i++) {
                    Human child = this.children.get(i);
                    text += ", " + child.name;
                }
            }
            return text;
        }
    }

}
