package com.codegym.task.task08.task0812;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Scanner;

/*
Longest sequence

*/
public class Solution {
    public static void main(String[] args) throws IOException {
        ArrayList<Integer> list = new ArrayList<Integer>();
        ArrayList<Integer> count = new ArrayList<Integer>();
        Scanner sc = new Scanner(System.in);

        for (int i = 0; i < 10; i++){
            list.add(sc.nextInt());
        }

        int c = 1;
        for (int i = 0; i < list.size() - 1; i++){
            if(list.get(i).equals(list.get(i+1))){
                c++;
            } else {
                count.add(c);
                c = 1;
            }
        }

        Collections.sort(count, Collections.reverseOrder());
        System.out.println(count.get(0));
    }
}