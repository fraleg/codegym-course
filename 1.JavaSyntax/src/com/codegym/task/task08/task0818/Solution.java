package com.codegym.task.task08.task0818;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* 
Only for the rich

*/

public class Solution {
    public static HashMap<String, Integer> createMap() {
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        map.put("Lewandowski", 20000);
        map.put("Olejniczak", 20000);
        map.put("Legowski", 20000);
        map.put("Kowalski", 200);
        map.put("Nowak", 3500);
        map.put("Grosicki", 12000);
        map.put("Krzynowek", 150);
        map.put("Piatek", 400);
        map.put("Glik", 30000);
        map.put("Piszczek", 17000);

        return map;
    }

    public static void removeItemFromMap(HashMap<String, Integer> map) {
        Iterator<Map.Entry<String, Integer>> it = map.entrySet().iterator();

        while (it.hasNext()){
            Map.Entry<String, Integer> pair = it.next();
            if (pair.getValue() < 500){
                it.remove();
            }
        }
    }

    public static void main(String[] args) {

    }
}