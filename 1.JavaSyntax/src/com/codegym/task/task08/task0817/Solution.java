package com.codegym.task.task08.task0817;

import java.util.*;

/* 
We don't need repeats

*/

public class Solution {
    public static HashMap<String, String> createMap() {
        HashMap<String, String> map = new HashMap<String, String>();

        map.put("Lewandowski", "Robert");
        map.put("Grosicki", "Kamil");
        map.put("Piatek", "Krzysztof");
        map.put("Milik", "Arek");
        map.put("Krychowiak", "Grzegorz");
        map.put("Glik", "Kamil");
        map.put("Jedrzejczyk", "Artur");
        map.put("Blaszczykowski", "Jakub");
        map.put("Peszko", "Slawomir");
        map.put("Maczynski", "Krzysztof");

        return map;
    }

    public static void removeFirstNameDuplicates(Map<String, String> map) {
        ArrayList<String> NewList = new ArrayList<>();
        HashMap<String, String> Copy = new HashMap<>(map);

        for (Map.Entry<String,String> pair : Copy.entrySet()){
            if (!NewList.contains(pair.getValue())){
                NewList.add(pair.getValue());
            } else {
                removeItemFromMapByValue(map, pair.getValue());
            }
        }
    }

    public static void removeItemFromMapByValue(Map<String, String> map, String value) {
        HashMap<String, String> copy = new HashMap<String, String>(map);
        for (Map.Entry<String, String> pair : copy.entrySet()) {
            if (pair.getValue().equals(value))
                map.remove(pair.getKey());
        }
    }

    public static void main(String[] args) {

    }
}
