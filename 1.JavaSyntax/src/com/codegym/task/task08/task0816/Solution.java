package com.codegym.task.task08.task0816;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/* 
Kind Emma and the summer holidays

*/

public class Solution {
    public static HashMap<String, Date> createMap() throws ParseException {
        DateFormat df = new SimpleDateFormat("MMMMM d yyyy", Locale.ENGLISH);
        HashMap<String, Date> map = new HashMap<String, Date>();
        map.put("Stallone", df.parse("JUNE 10 1980"));
        map.put("Seagal", df.parse("JANUARY 10 1985"));
        map.put("Legowski", df.parse("FEBRUARY 10 1981"));
        map.put("Grzywacz", df.parse("JULY 10 1995"));
        map.put("Piatek", df.parse("FEBRUARY 10 1999"));
        map.put("Lewandowski", df.parse("JANUARY 10 2000"));
        map.put("Ronaldo", df.parse("JULY 10 2010"));
        map.put("Messi", df.parse("FEBRUARY 10 2012"));
        map.put("Pogba", df.parse("JANUARY 10 1990"));
        map.put("Dzban", df.parse("JULY 10 1970"));

        return map;
    }

    public static void removeAllSummerPeople(HashMap<String, Date> map) throws ParseException {
        DateFormat df = new SimpleDateFormat("MMMMM d", Locale.ENGLISH);
        //Date begin = df.parse("JUNE 1");
        //Date end = df.parse("AUGUST 31");
        Iterator<Map.Entry<String, Date>> it = map.entrySet().iterator();

        while (it.hasNext()){
            Map.Entry<String, Date> pair = it.next();
            //String name = pair.getKey();
            Date data = pair.getValue();
            if (data.getMonth() == 5 || data.getMonth() == 6 || data.getMonth() == 7){
                it.remove();
            }
        }
    }

    public static void main(String[] args) throws ParseException {
//        HashMap<String, Date> map = createMap();
//        removeAllSummerPeople(map);
//
//        Iterator<Map.Entry<String, Date>> iterator = map.entrySet().iterator();
//        while (iterator.hasNext())
//        {
//            // Get a key-value pair
//            Map.Entry<String, Date> pair = iterator.next();
//            String key = pair.getKey();            // Key
//            Date value = pair.getValue();        // Value
//            System.out.println(key + "\t-\t" + value);
//        }
    }
}
