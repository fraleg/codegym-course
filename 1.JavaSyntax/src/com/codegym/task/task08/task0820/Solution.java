package com.codegym.task.task08.task0820;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/* 
Animal set

*/

public class Solution {
    public static void main(String[] args) {
        Set<Cat> cats = createCats();
        Set<Dog> dogs = createDogs();

        Set<Object> pets = join(cats, dogs);
        printPets(pets);

        removeCats(pets, cats);
        printPets(pets);
    }

    public static Set<Cat> createCats() {
        HashSet<Cat> result = new HashSet<Cat>();

        result.add(new Cat("Mruczuś"));
        result.add(new Cat("Kiciuś"));
        result.add(new Cat("Burek"));
        result.add(new Cat("Stefan"));

        return result;
    }

    public static Set<Dog> createDogs() {
        HashSet<Dog> result = new HashSet<Dog>();

        result.add(new Dog("Azor"));
        result.add(new Dog("Bary"));
        result.add(new Dog("Basko"));

        return result;
    }

    public static Set<Object> join(Set<Cat> cats, Set<Dog> dogs) {
        HashSet<Object> pets = new HashSet<>();
        pets.addAll(cats);
        pets.addAll(dogs);
        return pets;
    }

    public static void removeCats(Set<Object> pets, Set<Cat> cats) {
        pets.removeAll(cats);
    }

    public static void printPets(Set<Object> pets) {
        Iterator<Object> it = pets.iterator();

        while (it.hasNext()){
            Object obj = it.next();
            System.out.println(obj);
        }
    }

    public static class Cat{

        String name;

        public Cat(){
            this.name = name;
        }

        public Cat(String name){
            this.name = name;
        }
    }

    public static class Dog{

        String name;

        public Dog(){
            this.name = name;
        }

        public Dog(String name){
            this.name = name;
        }
    }
}
