package com.codegym.task.task04.task0419;

/* 
Maximum of four numbers

*/

import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        int d = scanner.nextInt();

        int max1, max2;
        if(a > b){
            max1 = a;
        } else if(a < b){
            max1 = b;
        } else{
            max1 = a;
        }

        if(c > d){
            max2 = c;
        } else if(c < d){
            max2 = d;
        } else{
            max2 = c;
        }

        if(max1 > max2){
            System.out.println(max1);
        } else if(max1 < max2){
            System.out.println(max2);
        } else{
            System.out.println(max1);
        }
    }
}
