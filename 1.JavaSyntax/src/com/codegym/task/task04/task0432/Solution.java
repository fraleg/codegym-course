package com.codegym.task.task04.task0432;



/* 
You can't have too much of a good thing

*/

import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        String phrase = sc.nextLine();
        int number = sc.nextInt();

        while(number >= 1){
            System.out.println(phrase);
            number--;
        }
    }
}
