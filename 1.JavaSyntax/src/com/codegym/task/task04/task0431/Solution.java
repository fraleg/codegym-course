package com.codegym.task.task04.task0431;

/* 
From 10 to 1

*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        int n = 10;
        while(n >= 1){
            System.out.println(n);
            n--;
        }
    }
}
