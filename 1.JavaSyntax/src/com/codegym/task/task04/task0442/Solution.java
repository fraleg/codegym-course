package com.codegym.task.task04.task0442;


/* 
Adding

*/

import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
        int sum = 0;
        Scanner sc = new Scanner(System.in);
        while (true) {
            int number = sc.nextInt();
            sum += number;
            if (number == -1) {
                System.out.println(sum);
                break;
            }
        }
    }
}
