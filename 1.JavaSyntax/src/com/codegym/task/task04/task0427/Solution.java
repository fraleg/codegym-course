package com.codegym.task.task04.task0427;

/* 
Describing numbers

*/

import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();

        if(a >=1 && a <=9 && a % 2 == 0){
            System.out.println("even single-digit number");
        } else if(a >=1 && a <=9 && a % 2 != 0){
            System.out.println("odd single-digit number");
        } else if(a >=10 && a <=99 && a % 2 == 0){
            System.out.println("even two-digit number");
        } else if(a >=10 && a <=99 && a % 2 != 0){
            System.out.println("odd two-digit number");
        } else if(a >=100 && a <=999 && a % 2 == 0){
            System.out.println("even three-digit number");
        } else if(a >=100 && a <=999 && a % 2 != 0){
            System.out.println("odd three-digit number");
        }

    }
}
