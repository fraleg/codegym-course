package com.codegym.task.task04.task0429;

/* 
Positive and negative numbers

*/

import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        int positive = 0;
        int negative = 0;

        int a = sc.nextInt();
        if(a > 0) positive++;
        else if(a < 0) negative++;

        int b = sc.nextInt();
        if(b > 0) positive++;
        else if(b < 0) negative++;

        int c = sc.nextInt();
        if(c > 0) positive++;
        else if(c < 0) negative++;

        System.out.println("Number of negative numbers: " + negative);
        System.out.println("Number of positive numbers: " + positive);
    }
}
