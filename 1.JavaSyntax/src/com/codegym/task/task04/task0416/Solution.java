package com.codegym.task.task04.task0416;

/* 
Crossing the road blindly

*/

import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        double number = sc.nextDouble();

        if((number % 5 >= 0) && (number % 5 < 3)){
            System.out.println("green");
        } else if((number % 5 >= 3) && (number % 5 < 4)){
            System.out.println("yellow");
        } else if((number % 5 >= 4) && (number % 5 < 5)){
            System.out.println("red");
        }
    }
}