package com.codegym.task.task04.task0441;


/* 
Somehow average

*/
import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt(); // 2
        int b = sc.nextInt(); // 3
        int c = sc.nextInt(); // 1

        if(a >= b && b >= c){
            System.out.println(b);
        } else if(a >= c && c >= b){
            System.out.println(c);
        } else if(b >= a && a >= c){
            System.out.println(a);
        } else if(b >= c && c >= a){
            System.out.println(c);
        } else if(c >= a && a >= b){
            System.out.println(a);
        } else if(c >= b && b >= a){
            System.out.println(b);
        }
    }
}
