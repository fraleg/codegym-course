package com.codegym.task.task04.task0428;

/* 
Positive number

*/

import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        int count = 0;
        int a = sc.nextInt();
        if(a > 0) count++;
        int b = sc.nextInt();
        if(b > 0) count++;
        int c = sc.nextInt();
        if(c > 0) count++;


        System.out.println(count);


    }
}
