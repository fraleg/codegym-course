package com.codegym.task.task04.task0424;

/* 
Three numbers

*/

import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sc.nextInt();
        int c = sc.nextInt();

        if(a == b && b != c){
            System.out.println("3");
        } else if(a == c && b != a){
            System.out.println("2");
        } else if(b == c && a != b){
            System.out.println("1");
        }
    }
}
