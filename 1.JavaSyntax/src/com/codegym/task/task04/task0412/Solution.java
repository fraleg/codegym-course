package com.codegym.task.task04.task0412;

/* 
Positive and negative numbers

*/

import java.io.*;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        if(n > 0){
            System.out.println(2*n);
        } else if(n == 0){
            System.out.println(n);
        } else if(n < 0){
            System.out.println(n+1);
        }

    }

}