package com.codegym.task.task01.task0132;

/* 
Sum of the digits of a three-digit number

*/

public class Solution {
    public static void main(String[] args) {
        System.out.println(sumDigitsInNumber(546));
    }

    public static int sumDigitsInNumber(int number) {
        int jednosci = number % 10;
        number = number / 10;
        int dziesiatki = number % 10;
        number = number / 10;
        int setki = number % 10;
        return setki + dziesiatki + jednosci;
    }
}