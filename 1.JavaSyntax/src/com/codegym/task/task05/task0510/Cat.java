package com.codegym.task.task05.task0510;

/* 
Initializing cats

*/

public class Cat {

    String name;
    int weight;
    int age;
    String color;
    String address;

    public void initialize(String name){
        this.name = name;
        this.weight = 5;
        this.age = 3;
        this.color = "Black";
    }

    public void initialize(String name, int weight, int age){
        this.name = name;
        this.weight = weight;
        this.age = age;
        this.color = "Black";
    }

    public void initialize(String name, int age){
        this.name = name;
        this.age = age;
        this.weight = 5;
    }

    public void initialize(int weight, String color){
        this.weight = weight;
        this.color = color;
        this.age = 3;
    }

    public void initialize(int weight, String color, String address){
        this.weight = weight;
        this.color = color;
        this.address = address;
        this.age = 3;
    }

    public static void main(String[] args) {

    }
}
