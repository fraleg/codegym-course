package com.codegym.task.task05.task0502;

/* 
Implement the fight method

*/

public class Cat {
    public int age;
    public int weight;
    public int strength;

    public Cat() {
    }

    public boolean fight(Cat anotherCat) {
        boolean score = true;
        if(this.strength > anotherCat.strength){
            score = true;
        } else if(this.strength == anotherCat.strength) {
            if(this.weight < anotherCat.weight){
                score =  true;
            } else if(this.weight == anotherCat.weight) {
                if(this.age < anotherCat.age){
                    score = true;
                } else {
                    return false;
                }
            } else{
                score =  false;
            }
        } else if(this.strength < anotherCat.strength){
            score =  false;
        }
        return score;
    }

    public static void main(String[] args) {

    }
}
