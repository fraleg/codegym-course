package com.codegym.task.task05.task0528;

/* 
Display today's date

*/

public class Solution {
    public static void main(String[] args) {
        Date date = new Date(11, 4, 2019);
        System.out.println(date.month + " " + date.day + " " + date.year);
    }

    public static class Date{
        int day;
        int month;
        int year;

        public Date(int day, int month, int year){
            this.day = day;
            this.month = month;
            this.year = year;
        }
    }
}
