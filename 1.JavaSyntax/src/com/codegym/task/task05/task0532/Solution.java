package com.codegym.task.task05.task0532;

import java.io.*;

/* 
Task about algorithms

*/

public class Solution {
    private String name;

    public Solution(String name) {
        this.name = name;
    }

    public Solution() {
    }

    public static void main(String[] args) throws Throwable {

        for (int i = 0 ; i < 1000000; i++) {

            Solution cat = new Solution();
            cat = null;// The first object becomes available for garbage collection here
        }
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("The Cat is destroyed!");
    }
}
