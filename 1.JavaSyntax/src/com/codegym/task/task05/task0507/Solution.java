package com.codegym.task.task05.task0507;

/* 
Arithmetic mean

*/

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        int number;
        int sum = 0, count = 0;

        while (true) {
            number = sc.nextInt();
            if (number == -1){
                System.out.println("Suma: " + sum + "  Liczb: " + count);
                System.out.println((double)sum/count);
                break;
            }
            sum += number;
            count++;
        }
    }
}

