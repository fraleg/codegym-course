package com.codegym.task.task03.task0325;

import java.io.*;
import java.util.Scanner;

/* 
Financial expectations

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();
        System.out.println("I will earn $" + n + " per hour");
    }
}
