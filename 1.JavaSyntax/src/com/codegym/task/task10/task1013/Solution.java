package com.codegym.task.task10.task1013;

/* 
Human class constructors

*/

public class Solution {
    public static void main(String[] args) {
    }

    public static class Human {

        private String name;
        private String lastName;
        private int age;
        private boolean sex;
        private int height;
        private int weight;

        public Human(String name, String lastname, int age, boolean sex, int height, int weight){
            this.name = name;
            this.lastName = lastname;
            this.age = age;
            this.sex = sex;
            this.height = height;
            this.weight = weight;
        }

        public Human(String name, String lastname, int age, boolean sex, int height){
            this.name = name;
            this.lastName = lastname;
            this.age = age;
            this.sex = sex;
            this.height = height;
            this.weight = 70;
        }

        public Human(String name, String lastname, int age, boolean sex){
            this.name = name;
            this.lastName = lastname;
            this.age = age;
            this.sex = sex;
            this.height = 180;
            this.weight = 70;
        }

        public Human(String name, String lastname, int age){
            this.name = name;
            this.lastName = lastname;
            this.age = age;
            this.sex = true;
            this.height = 180;
            this.weight = 70;
        }

        public Human(String name, String lastname){
            this.name = name;
            this.lastName = lastname;
            this.age = 25;
            this.sex = true;
            this.height = 180;
            this.weight = 70;
        }

        public Human(String name){
            this.name = name;
            this.lastName = "Kowalski";
            this.age = 25;
            this.sex = true;
            this.height = 180;
            this.weight = 70;
        }

        public Human(){
            this.name = "Jan";
            this.lastName = "Kowalski";
            this.age = 25;
            this.sex = true;
            this.height = 180;
            this.weight = 70;
        }

        public Human(String name, int age){
            this.name = name;
            this.lastName = "Kowalski";
            this.age = age;
            this.sex = true;
            this.height = 180;
            this.weight = 70;
        }

        public Human(String name, String lastName, boolean sex){
            this.name = name;
            this.lastName = lastName;
            this.age = 25;
            this.sex = sex;
            this.height = 180;
            this.weight = 70;
        }

        public Human(String name, boolean sex){
            this.name = name;
            this.lastName = "Kowalski";
            this.age = 25;
            this.sex = sex;
            this.height = 180;
            this.weight = 70;
        }
    }
}
