package com.codegym.task.task10.task1015;

import java.util.ArrayList;
import java.util.List;

/* 
Array of string lists

*/

public class Solution {
    public static void main(String[] args) {
        ArrayList<String>[] arrayOfStringList = createList();
        printList(arrayOfStringList);
    }

    public static ArrayList<String>[] createList() {
        ArrayList<String>[] result = new ArrayList[10];
        for (int j=0; j<result.length; j++) {
            ArrayList<String> list = new ArrayList<>();
            for (int i=0; i<10; i++) {
                list.add("345");
            }
            result[j]=list;
        }
        return result;
    }

    public static void printList(ArrayList<String>[] arrayOfStringList) {
        for (ArrayList<String> list : arrayOfStringList) {
            for (String s : list) {
                System.out.println(s);
            }
        }
    }
}