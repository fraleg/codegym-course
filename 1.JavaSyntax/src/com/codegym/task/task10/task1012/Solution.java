package com.codegym.task.task10.task1012;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* 
Number of letters

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        // Alphabet
        String abc = "abcdefghijklmnopqrstuvwxyz";
        char[] abcArray = abc.toCharArray();

        ArrayList<Character> alphabet = new ArrayList<>();
        for (char letter : abcArray) {
            alphabet.add(letter);
        }

        // Read in strings
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            String s = reader.readLine();
            list.add(s.toLowerCase());
        }

        // create map and initialize it
        HashMap<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < abc.length(); i++){
            map.put(abc.charAt(i), 0);
        }
        //map.put('a', map.get('a') + 1);
        //System.out.println(map.get('a'));

        // go through list of words
        for (int i = 0; i < list.size(); i++){
            // go thorugh every letter of every word
            for (int j = 0; j < list.get(i).length(); j++){
                if (map.containsKey(list.get(i).charAt(j))) {
                    map.put(list.get(i).charAt(j), map.get(list.get(i).charAt(j)) + 1);
                }
            }
        }

        Iterator<Map.Entry<Character, Integer>> it = map.entrySet().iterator();
        while (it.hasNext()){
            Map.Entry<Character, Integer> pair = it.next();
            Character ch = pair.getKey();
            Integer val = pair.getValue();
            System.out.println(ch + " " + val);
        }

    }

}
